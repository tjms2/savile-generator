package savile;
import java.lang.Runtime;
import java.lang.Process;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.io.IOException;

public class BugFinder {

	static Model m;
	static ArrayList<String> savileErrorOutput = new ArrayList<String>();

	//waits this long for savile but no longer. this program doesn't care if the model is solvable
	static long maximumTimeToWait = 10000;//5 seconds in milliseconds

	public static void main(String[] args) {
		for(int i = 0; i < 10; i++) {
			do{
				m = Model.genModel();
				clear();
				m.print();
				store(m);
				savileErrorOutput = runSavile();
			} while(savileErrorOutput.size() <= 2);
			TokenQueue.add(m);
			while(!TokenQueue.isEmpty()){
				filterIrrelevantTokens(TokenQueue.next());
			}
			archive(m, i);
		}
	}
	
	public static void filterIrrelevantTokens(Token t) {
		//t is not defaulted.
		//attempt to default t and check if sameBug.
		t.resetToDefault();
		if(!sameBug()){
			t.unDefault();	
		} else {
			clear();
			m.print();
		}
		ArrayList<Token> children = t.children();
		for(int i = 0; i < t.children().size(); i++) {
			TokenQueue.add(children.get(i));
		}
	}

	public static void clear() {
			System.out.print("\033[H\033[2J"); 
			System.out.flush();		
	}

	public static boolean sameBug() {
		//parses m into  a file
		store(m);
		//runs savile
		ArrayList<String> newErrorOutput = runSavile();
		//checks new output against previous output (so savile must be run once before this is called)
		if(newErrorOutput.size() <= 2 || savileErrorOutput.size() <= 2) return false;//just to prevent exceptions, this shouldn't ever run in theory...
		for(int i = 0; i < 3; i++) {
			if(!savileErrorOutput.get(i).equals(newErrorOutput.get(i))) {
				return false;
			}
		}
		return true;
	}

	//unneccessary, but is here to make the above code easier to read as it is a little wordy...
	public static void store(Model m) {
		try {
			Storage.storeModel(m);
		} catch(IOException e) {
			System.out.println("ERROR: storage failed");
		}
	}

	public static void archive(Model m, int num) {
		try {
			Storage.storeBadModel(m, num);
		} catch(IOException e) {
			System.out.println("ERROR: storage of bad model failed");
		}
	}

	//generates savile output and returns the error output (could be nothing)
	public static ArrayList<String> runSavile() {
		SavileRunner runner = new SavileRunner();
		
		runner.start();
		try {
		    runner.join(maximumTimeToWait);
		} catch(InterruptedException e) {
		    Thread.currentThread().interrupt();
		}
		if(runner.isAlive()) {
			runner.interrupt();
			return new ArrayList<String>();
		}
		return runner.savileErrorOutput;

	}
}