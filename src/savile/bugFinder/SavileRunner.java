package savile;
import java.lang.Runtime;
import java.lang.Process;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.io.IOException;

public class SavileRunner extends Thread {
	public ArrayList<String> savileErrorOutput = new ArrayList<String>();
	public String pathToSavile = "SavileRow/stacs_cp-savilerow-310a9e8e93bc/savilerow";

	@Override
	public void run() {
		try {
			Runtime rt = Runtime.getRuntime();
			String[] commands = {"./../../"+pathToSavile, "../savileFiles/test.eprime", "../savileFiles/test.param", "-O0", "-run-solver","-solutions-to-stdout"};
			long time = System.currentTimeMillis();
			Process proc = rt.exec(commands);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			BufferedReader stdError = new BufferedReader(new  InputStreamReader(proc.getErrorStream()));

			String s = null;
			ArrayList<String> newSavileErrorOutput = new ArrayList<String>();
			while ((s = stdError.readLine()) != null) {
			    newSavileErrorOutput.add(s);
			}
			this.savileErrorOutput = newSavileErrorOutput;
		} catch(IOException e) {
			System.out.println("ERROR: runSavile failure. fly you fools!");
			e.printStackTrace();
		}
	}
}