package savile;
import java.util.ArrayList;
public class TokenQueue   {

	/*
	This has been updated to a stack. it is no longer a queue.
	If I wanted to be pernickety, it is a FILO queue. (CS2001 paying off... I think)
	*/

	private static ArrayList<Token> q = new ArrayList<Token>();

	public static Token next() {
		return (q.size() > 0)?  q.remove(q.size()-1) : null;
	}

	public static Token peek() {
		return (q.size() > 0)?  q.get(q.size()-1) : null;
	}

	public static void add(Token t) {
		q.add(t);
	}

	public static boolean isEmpty() {
		return q.size()==0;
	}
}