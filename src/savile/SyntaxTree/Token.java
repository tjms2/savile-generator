package savile;
import java.util.ArrayList;
public abstract class Token extends Generatable {
	
	public abstract String toString();
	public abstract String toDefaultString();

	protected boolean defaulted = false;
	public void resetToDefault() {	
		defaulted = true;
	}
	public void unDefault() {
		defaulted = false;
	}
	public abstract ArrayList<Token> children();
}