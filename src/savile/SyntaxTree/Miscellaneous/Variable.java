package savile;
import java.util.ArrayList;
public class Variable extends Generatable {

	//categories
	public static final int DECISION = 1;
	public static final int QUANTIFIER = 2;
	public static final int PARAMETER = 3;
	public static final int CONSTANT = 4;
	public static final int[] CATEGORIES = new int[]{DECISION,QUANTIFIER,PARAMETER,CONSTANT};

	public int type;
	public int category;
	public String name;
	public DomainExpression d;
	public Expression e;//if the category is constant then this will be the value to substitute; if it is a decision then it will need a substitution



	public Variable(String n, int t, int c, DomainExpression d) {
		name = n;
		type = t;
		category = c;
		this.d = d;
	}

	public Variable(String n, int t, int c, Expression e) {
		name = n;
		type = t;
		category = c;
		this.e = e;
	}


	public Variable(String n, int t, int c, Expression e, DomainExpression d) {
		name = n;
		type = t;
		category = c;
		this.d = d;
		this.e = e;
	}

	public Expression evaluateToExpression(Substitution subs) {
		switch(category) {
			case DECISION:
				switch(type) {
					case Expression.INTEGER:
						IntEvaluation eval = subs.getInt(this);
						if(eval == null || eval.undefined) return null;
						return new IntegerLiteral(eval.value);
					case Expression.BOOLEAN:
					Boolean evalB = subs.getBoolean(this);
					if(evalB == null) return null;
					return new BooleanLiteral(evalB);
					case Expression.MATRIX:
						return subs.getMatrix(this);
				}
				break;
			case CONSTANT:
				return e;
			case PARAMETER:
			case QUANTIFIER:
			default:
			//TODO
				return null;
		}
		return null;
	}

	public String getTypeString() {
		switch(type) {
			case Expression.INTEGER :
				return "int";
			case Expression.BOOLEAN :
				return "bool";
			case Expression.MATRIX :
				return "matrix";
		}
		return "ERROR";//TODO actually handle this error case
	}

	/*
		Generation
	*/


	public static Variable genParameter(ArrayList<Variable> variablesInScope) {
		int t = Expression.genType();
		return new Variable(genVarName(variablesInScope), t, Variable.PARAMETER, DomainExpression.genDomainExpression(t, true, variablesInScope, 0));
	}

	public static Variable genQuantifierVariable(ArrayList<Variable> variablesInScope, DomainExpression d) {
		return new Variable(genVarName(variablesInScope), d.type, Variable.QUANTIFIER, d);
	}

	public static Variable genQuantifierVariable(ArrayList<Variable> variablesInScope, int depth) {
		int t = Expression.genType();
		return new Variable(genVarName(variablesInScope), t, Variable.QUANTIFIER, DomainExpression.genDomainExpression(t, false, variablesInScope, depth));
	}

	public static Variable genDecisionVariable(ArrayList<Variable> variablesInScope, DomainExpression d) {
		return new Variable(genVarName(variablesInScope), d.type, Variable.DECISION, d);
	}

	public static Variable genMatrixConstant(ArrayList<Variable> variablesInScope) {
		MatrixExpression me = MatrixExpression.genMatrixExpression(0,variablesInScope,true,true);
		return new Variable(genVarName(variablesInScope), Expression.MATRIX, Variable.CONSTANT, me);
	}

	public static Variable genIntConstant(ArrayList<Variable> variablesInScope) {
		IIntExpression ie = IntExpression.genIntExpression(0, variablesInScope, true);
		return new Variable(genVarName(variablesInScope), Expression.INTEGER, Variable.CONSTANT, ie);
	}

	public static Variable genBoolConstant(ArrayList<Variable> variablesInScope) {
		BooleanExpression be = BooleanExpression.genBooleanExpression(0, variablesInScope, true);
		return new Variable(genVarName(variablesInScope), Expression.BOOLEAN, Variable.CONSTANT, be);
	}

	public static boolean keyword(String n) {
		return 	n.equals("")||
				n.equals("or")||
				n.equals("in")||
				n.equals("given")||
				n.equals("find")||
				n.equals("be")||
				n.equals("in")||
				n.equals("of")||
				n.equals("bool")||
				n.equals("false")||
				n.equals("true")||
				n.equals("int")||
				n.equals("matrix")||
				n.equals("letting");//TODO finish this for all words in language
	}

	public static boolean duplicate(String n, ArrayList<Variable> variablesInScope) {
		for(int i = 0; i < variablesInScope.size(); i++) {
			if(n.equals(variablesInScope.get(i).name)) return true;
		}
		return false;
	}

	public static boolean validName(String n, ArrayList<Variable> variablesInScope) {
		return (!keyword(n))&&(!duplicate(n, variablesInScope));
	}

	public static String genVarName(ArrayList<Variable> variablesInScope) {
		String var = "";
		while(!validName(var, variablesInScope)) {
			var = "" + randomLetterChar();
			for(int i = 0; i < r.nextInt(Configurations.MAX_REPETITIONS);i++) var += randomVarChar();
		}
		return var;
	}

	public static char randomLetterChar() {
		int offset = r.nextBoolean() ? 65 : 97;
		return (char) (offset + r.nextInt(26));
	}

	public static char randomVarChar() {
		int theChar = 2345678;//invalid char
		while(!((theChar==137)||((theChar>=48)&&(theChar<=57))||((theChar>=97)&&(theChar<=122))||((theChar>=65)&&(theChar<=90)))){
			theChar = r.nextInt(123);
		}
		return (char) theChar;
	}
}