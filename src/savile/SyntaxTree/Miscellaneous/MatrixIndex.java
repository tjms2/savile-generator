package savile;
import java.util.ArrayList;
public class MatrixIndex extends Token {
	public boolean defined;
	public IIntExpression val;//if the matrix Index is defined
	
	public MatrixIndex() {
		defined = false;
	}

	public MatrixIndex(IIntExpression ie) {
		defined = true;
		val = ie;
	}
	
	public String toString() {
		if(defaulted) return toDefaultString();
		if(defined)
			return val.toString();
		return "..";
	}

	public String toDefaultString() {
		return IntExpression.DEFAULTINT;
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(defined && !defaulted) {
			children.add(val);
		}
		return children;
	}
}