package savile;
import java.util.ArrayList;
public class IntVariable extends IntExpression {

	Variable v;

	public IntVariable(Variable v) {
		this.v = v;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return v.name;
	}

	public String toDefaultString() {
		return DEFAULTINT;
	}

	public IntEvaluation evaluateInt(Substitution subs) {
		Expression e = v.evaluateToExpression(subs);
		if(e!=null) return e.evaluateInt(subs);
		return new IntEvaluation();
	}

	public ArrayList<Token> children() {
		return new ArrayList<Token>();
	}

	public static IntVariable genIntVariable(ArrayList<Variable> variablesInScope, boolean nonDecision) {
		ArrayList<Variable> intsInScope = filterToIntVariables(variablesInScope);
		if(nonDecision) intsInScope = filterDecisions(intsInScope);
		Variable v = intsInScope.get(r.nextInt(intsInScope.size()));
		return new IntVariable(v);
	}
}