package savile;
import java.util.ArrayList;
public class IntOperator extends IntExpression {
								 
	//types of operator
	public static final int PLUS = 0;
	public static final int MINUS = 1;
	public static final int MULTIPLY = 2;
	public static final int DIVIDE = 3;//always rounds down
	public static final int MODULO = 4;//note that this has an exact definition in the manual
	public static final int POWER = 5;
	public static final int MIN = 6;//not printed as an operator but has the functionality of one
	public static final int MAX = 7;//same as above
	public static final int[] TYPES = new int[] {PLUS,MINUS,MULTIPLY,DIVIDE,MODULO,POWER,MIN,MAX};

	public IIntExpression left;
	public IIntExpression right;
	public int operator;

	public IntOperator(int o, IIntExpression l, IIntExpression r) {
		left = l;
		right = r;
		operator = 0;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		switch(operator) {
			case PLUS :
			return "(" + left.toString() + "+" + right.toString() + ")";
			case MINUS :
			return "(" + left.toString() + "-" + right.toString() + ")";
			case MULTIPLY :
			return "(" + left.toString() + "*" + right.toString() + ")";
			case DIVIDE :
			return "(" + left.toString() + "/" + right.toString() + ")";
			case MODULO :
			return "(" + left.toString() + "%" + right.toString() + ")";
			case POWER :
			return "(" + left.toString() + "**" + right.toString() + ")";
			case MIN :
			return "min("+ left.toString() + ", " + right.toString() + ")";
			case MAX :
			return "max("+ left.toString() + ", " + right.toString() + ")";
			default:
			return "{IntOperator_Invalid_Operator_ERROR}";
		}
	}

	public String toDefaultString() {
		return right.toString();
	}

	//overriding IIntExpression
	public IntEvaluation evaluateInt(Substitution subs) {
		IntEvaluation leftEval = left.evaluateInt(subs);
		IntEvaluation rightEval = right.evaluateInt(subs);
		if(leftEval.undefined || rightEval.undefined) return new IntEvaluation();
		int leftVal = leftEval.value;
		int rightVal = rightEval.value;
		switch(operator) {	
			case PLUS :
			return new IntEvaluation(leftVal+rightVal);
			case MINUS :
			return new IntEvaluation(leftVal-rightVal);
			case MULTIPLY :
			return new IntEvaluation(leftVal*rightVal);
			case DIVIDE :
			return new IntEvaluation(leftVal/rightVal);//TODO check if this is rounding correctly
			case MODULO :
			return new IntEvaluation(leftVal%rightVal);
			case POWER :
			return new IntEvaluation(leftVal^rightVal);
			case MIN :
			return new IntEvaluation((leftVal>rightVal) ? rightVal : leftVal);
			case MAX :
			return new IntEvaluation((leftVal>rightVal) ? leftVal : rightVal);
			default:
			return new IntEvaluation();//TODO catch this
		}
	}

	public ArrayList<Token> children() {
		ArrayList<Token> kids = new ArrayList<Token>();
		kids.add(right);
		if(!defaulted) kids.add(left);
		return kids;
	}

	public static IntOperator genIntOperator(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		return new IntOperator(randOperator(), genIntExpression(depth+1, variablesInScope, nonDecision), genIntExpression(depth+1, variablesInScope, nonDecision));
	}

	private static int randOperator() {
		return TYPES[r.nextInt(TYPES.length)];
	}
}