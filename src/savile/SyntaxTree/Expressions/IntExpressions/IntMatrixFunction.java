package savile;
import java.util.ArrayList;
public class IntMatrixFunction extends IntExpression {

	public static final int MAX = 7;
	public static final int MIN = 8;
	public static final int SUM = 10;
	public static final int PRODUCT = 11;
	public static final int[] TYPES = new int[] {MAX,MIN,SUM,PRODUCT};

	public int type;
	public MatrixExpression m;

	public IntMatrixFunction(int t, MatrixExpression m) {
		this.type = t;
		this.m = m;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		switch(type) {
			case MAX:
			return ("max(" + m.toString() + ")");
			case MIN:
			return ("min(" + m.toString() + ")");
			case SUM:
			return ("sum(" + m.toString() + ")");
			case PRODUCT:
			return ("product(" + m.toString() + ")");
			default:
			return "{IntMatrixFunction_ERROR_INVALIDTYPE}";
		}
	}
	public String toDefaultString(){
		return DEFAULTINT;
	}

	public IntEvaluation evaluateInt(Substitution subs) {
		return new IntEvaluation();//TODO
	}

	public ArrayList<Token> children() {
		ArrayList<Token> childs = new ArrayList<Token>();
		if(defaulted) return childs;
		childs.add(m);
		return childs;
	}

	public static IntMatrixFunction genIntMatrixFunction(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		MatrixExpression m = MatrixExpression.genMatrixExpression(depth+1, variablesInScope, nonDecision, false);
		int t = randIntMatrixFunction();
		return new IntMatrixFunction(t, m);
	}

	public static int randIntMatrixFunction() {
		return TYPES[r.nextInt(TYPES.length)];
	}
}