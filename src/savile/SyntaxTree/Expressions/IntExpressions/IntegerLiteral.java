package savile;
import java.util.ArrayList;
public class IntegerLiteral extends IntExpression {

	int val;

	public IntegerLiteral(int i) {
		this.val = i;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "" + val;
	}

	public String toDefaultString() {
		return DEFAULTINT;
	}

	public IntEvaluation evaluateInt(Substitution subs) {
		return new IntEvaluation(val);
	}

	public ArrayList<Token> children() {
		return new ArrayList<Token>();
	}

	public static IntegerLiteral genIntegerLiteral() {
		return new IntegerLiteral(r.nextInt(Configurations.MAX_REPETITIONS));
	}
}