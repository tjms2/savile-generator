package savile;
import java.util.ArrayList;
public class IntUnaryOperator extends IntExpression {

	//unary operators
	public static final int NEGATION = 6;
	public static final int ABSOLUTE = 7;
	public static final int FACTORIAL = 8;
	public static final int POPCOUNT = 9;
	public static final int[] TYPES = new int[]{NEGATION,ABSOLUTE,FACTORIAL,POPCOUNT};

	public int operator;
	public IIntExpression exp;

	public IntUnaryOperator(IIntExpression e, int o) {
		exp = e;
		operator = o;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		switch(operator) {
			case NEGATION :
				return "(-" + exp.toString() + ")";
			case ABSOLUTE :
				return "|" + exp.toString() + "|";
			case FACTORIAL:
				return "factorial(" + exp.toString() + ")";
			case POPCOUNT:
				return "popcount(" + exp.toString() + ")";
			default :
				return "{UnaryOperator_Invalid_Operator_ERROR}";
		}
	}

	public String toDefaultString() {
		return exp.toString();
	}

	public IntEvaluation evaluateInt(Substitution subs) {
		IntEvaluation subEval = exp.evaluateInt(subs);
		if(subEval.undefined) return subEval;
		int subVal = subEval.value;
		switch(operator) {
			case NEGATION:
				return new IntEvaluation(-subVal);
			case ABSOLUTE:
				return new IntEvaluation((subVal > 0) ? subVal : (-subVal));
			case FACTORIAL:
				return new IntEvaluation();///TODO
			case POPCOUNT:
				return new IntEvaluation();//TODO or not
			default:
				return new IntEvaluation();
		}
	}

	public ArrayList<Token> children() {
		ArrayList<Token> kids = new ArrayList<Token>();
		kids.add(exp);
		return kids;
	}

	public static IntUnaryOperator genIntUnaryOperator(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		return new IntUnaryOperator(genIntExpression(depth+1, variablesInScope, nonDecision), randOperator());
	}

	public static int randOperator() {
		return TYPES[r.nextInt(TYPES.length)];
	}
}