package savile;
import java.util.ArrayList;
public class QuantifiedSum extends IntExpression {

	public IIntExpression exp;
	public DomainExpression quantifiedSumVarsDomain;
	public ArrayList<Variable> quantifiedSumVars;

	public QuantifiedSum(IIntExpression exp, ArrayList<Variable> quantVars, DomainExpression dom) {
		this.exp = exp;
		quantifiedSumVars = quantVars;
		quantifiedSumVarsDomain = dom;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "(sum " + quantifiedSumVarsToString() + " : " + quantifiedSumVarsDomain.toString() + " . " + exp.toString() + ")";
	}

	public String quantifiedSumVarsToString(){
		String str = "";
		for(int i =0; i < quantifiedSumVars.size(); i++) {
			if(i!=0) str += ",";
			str += quantifiedSumVars.get(i).name;
		}
		return str;
	}

	public String toDefaultString() {
		return DEFAULTINT;
	}

	public IntEvaluation evaluateInt(Substitution subs) {
		return new IntEvaluation();//TODO
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(defaulted) return children;
		children.add(quantifiedSumVarsDomain);
		children.add(exp);
		return children;
	}

	public static QuantifiedSum genQuantifiedSum(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		ArrayList<Variable> newQuants = new ArrayList<Variable>();
		int numQuantsToGen = r.nextInt(Configurations.MAX_REPETITIONS);
		DomainExpression theQuantsdomain = DomainExpression.genDomainExpression(Expression.genType(), false, variablesInScope, depth+1);
		ArrayList<Variable> quants = variablesInScope;
		for(int i = -1; i < numQuantsToGen; i++) {
			Variable v = Variable.genQuantifierVariable(variablesInScope, theQuantsdomain);
			newQuants.add(v);
			quants = copyAdd(quants, v);
		}
		IIntExpression ie = genIntExpression(depth+1, quants, nonDecision);
		return new QuantifiedSum(ie, newQuants, theQuantsdomain);
	}
}