package savile;
import java.util.ArrayList;
public abstract class IntExpression extends IIntExpression {
	//types of int expression
	public static final int BOOLEANSUBSTITUTION = -1;
	public static final int LITERAL = 0;
	public static final int VARIABLE = 1;
	public static final int UNARY = 2;
	public static final int OPERATOR = 3;
	public static final int MATRIXINDEX = 6;
	public static final int INTMATRIXFUNCTION = 7;
	public static final int QUANTIFIEDSUM = 9;
	public static final int[] INTEXPRESSIONTYPES = new int[]{BOOLEANSUBSTITUTION,LITERAL,VARIABLE,UNARY,OPERATOR,MATRIXINDEX,QUANTIFIEDSUM};

	public static final String DEFAULTINT = "1";

	public int expressionType() {
		return INTEGER;
	}

	public static IIntExpression genIntExpression(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		switch(genValidIntExpressionType(depth,variablesInScope,nonDecision)) {
			case BOOLEANSUBSTITUTION:
				return BooleanExpression.genBooleanExpression(depth, variablesInScope, nonDecision);
			case LITERAL:
				return IntegerLiteral.genIntegerLiteral();
			case VARIABLE:
				return IntVariable.genIntVariable(variablesInScope, nonDecision);
			case UNARY:
				return IntUnaryOperator.genIntUnaryOperator(depth, variablesInScope, nonDecision);
			case OPERATOR:
				return IntOperator.genIntOperator(depth, variablesInScope, nonDecision);
			case MATRIXINDEX:
				return IntMatrixIndex.genIntMatrixIndex(depth, variablesInScope, nonDecision);
			case INTMATRIXFUNCTION:
				return IntMatrixFunction.genIntMatrixFunction(depth, variablesInScope, nonDecision);
			case QUANTIFIEDSUM:
				return QuantifiedSum.genQuantifiedSum(depth, variablesInScope, nonDecision);
			default: 
				System.out.println("genIntExpression, invalid expression type");
				return null;//catch this
		}
	}

	public static int genValidIntExpressionType(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		int type = randIntExpressionType();
		if(depth>=Configurations.RECURSION_DEPTH) {
			while(true){
			//must be atomic
				if(type==LITERAL || type == BOOLEANSUBSTITUTION) return type;
				if(type==VARIABLE){
					ArrayList<Variable> allVars = filterToIntVariables(variablesInScope);
					if(nonDecision) allVars = filterDecisions(allVars);
					if(allVars.size()>=1) return type;
				}
				type = randIntExpressionType();								
			}
		} else {
			while(true){
			//can be an expression
				if(type==VARIABLE){
					ArrayList<Variable> allVars = filterToIntVariables(variablesInScope);
					if(nonDecision) allVars = filterDecisions(allVars);
					if(allVars.size()>=1) return type;
				} else {
					if(type == MATRIXINDEX) {
						//check if there are any valid matrixes to index
						ArrayList<Variable> allVars = filterToIntMatrixVariables(variablesInScope);
						if(nonDecision) allVars = filterDecisions(allVars);
						if(allVars.size()>=1) return type;
					} else {
						return type;
					}
				}
				type = randIntExpressionType();
			}
		}

	}

	public static int randIntExpressionType() {
		return INTEXPRESSIONTYPES[r.nextInt(INTEXPRESSIONTYPES.length)];
	}

	protected static ArrayList<Variable> filterToIntMatrixVariables(ArrayList<Variable> vars) {
		ArrayList<Variable> newList = new ArrayList<Variable>();
		for(int i = 0;i<vars.size();i++) {
			Variable v = vars.get(i);
			if(v.type == Expression.MATRIX) {
				int baseType = -1;
				if(v.category==Variable.CONSTANT) {
					baseType = ((MatrixExpression) v.e).baseType();
				} else {
					baseType = v.d.matrixDomain.baseType;
				}
				if(baseType == Expression.INTEGER)
					newList.add(v);					
			}
		}
		return newList;
	}

	protected static ArrayList<Variable> filterToIntVariables(ArrayList<Variable> vars) {
		ArrayList<Variable> newList = new ArrayList<Variable>();
		for(int i = 0;i<vars.size();i++) {
			Variable v = vars.get(i);
			if(v.type == Expression.INTEGER) newList.add(v);
		}
		return newList;
	}
}