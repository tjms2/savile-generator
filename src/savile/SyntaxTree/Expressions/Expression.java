package savile;
import java.util.ArrayList;
public abstract class Expression extends Token {
	//to implement
	public abstract int expressionType();

	public MatrixEvaluation evaluateMatrix(Substitution subs) {
		System.out.println("Invalid Operation: evaluateMatrix called on non matrix");
		return null;
	}
	public boolean evaluateBoolean(Substitution subs){
		System.out.println("Invalid Operation: evaluateBoolean called on non boolean");
		return false;
	}
	public IntEvaluation evaluateInt(Substitution subs) {
		System.out.println("Invalid Operation: evaluateInt called on non int/bool");
		return null;
	}

	//types
	public static final int INTEGER = 0;
	public static final int BOOLEAN = 1;
	public static final int MATRIX = 2;
	public static final int[] EXPRESSIONTYPES = new int[]{INTEGER,BOOLEAN,MATRIX};


	public static Expression genExpression(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, int type) {
		switch(type) {
			case INTEGER :
				return IntExpression.genIntExpression(depth, variablesInScope, nonDecision);
			case BOOLEAN :
				return BooleanExpression.genBooleanExpression(depth, variablesInScope, nonDecision);
			case MATRIX :
				return MatrixExpression.genMatrixExpression(depth, variablesInScope, nonDecision, true);
			default :
				return null;
		}
	}

	public static int genType() {
		return EXPRESSIONTYPES[r.nextInt(EXPRESSIONTYPES.length)];
	}

}	