package savile;
import java.util.ArrayList;
public class Quantifier extends BooleanExpression {

	public static final int UNIVERSALQUANTIFIER = 1;
	public static final int EXISTENSIALQUANTIFIER = 2;
	public static final int[] QUANTIFIERTYPES = new int[] {UNIVERSALQUANTIFIER, EXISTENSIALQUANTIFIER};

	int quantifiedType;
	BooleanExpression exp;
	DomainExpression quantificationDomain;//if the type is a universal of existensial domain
	ArrayList<Variable> quantifiedVariables;

	public Quantifier(BooleanExpression b, DomainExpression dom, ArrayList<Variable> quants, int type) {
		exp = b;
		quantificationDomain = dom;
		quantifiedVariables = quants;
		quantifiedType = type;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "(" + quantType() + quantifiedVsToString() + " : " + quantificationDomain.toString() + " . " + exp.toString() + ")";
	}

	public String quantType() {
		if(quantifiedType==UNIVERSALQUANTIFIER) return "forall ";
		if(quantifiedType==EXISTENSIALQUANTIFIER) return "exists ";
		return "{Invalid_Quantifer_Type_ERROR}";
	}

	public String quantifiedVsToString() {
		String str = "";
		for(int i =0;i< quantifiedVariables.size(); i++) {
			if(i!=0)str+=",";
			str+= quantifiedVariables.get(i).name;
		}
		return str;
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(defaulted) return children;
		children.add(quantificationDomain);
		children.add(exp);
		return children;
	}

	public boolean evaluateBoolean(Substitution subs) {
		return false;//TODO
	}

	public static Quantifier genQuantifier(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		ArrayList<Variable> newQuants = new ArrayList<Variable>();
		int numQuantsToGen = r.nextInt(Configurations.MAX_REPETITIONS);
		DomainExpression theQuantsdomain = DomainExpression.genDomainExpression(Expression.genType(), false, variablesInScope, depth+1);
		ArrayList<Variable> quants = variablesInScope;
		for(int i = -1; i < numQuantsToGen; i++) {
			Variable v = Variable.genQuantifierVariable(variablesInScope, theQuantsdomain);
			newQuants.add(v);
			quants = copyAdd(quants, v);
		}
		BooleanExpression be = genBooleanExpression(depth+1, quants, nonDecision);
		int type = r.nextBoolean() ? UNIVERSALQUANTIFIER : EXISTENSIALQUANTIFIER;
		return new Quantifier(be, theQuantsdomain, newQuants, type);
	}
}