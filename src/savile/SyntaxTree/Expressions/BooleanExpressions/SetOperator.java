package savile;
import java.util.ArrayList;
public class SetOperator extends BooleanExpression {

	IIntExpression ie;
	Set s;

	public SetOperator(IIntExpression ie, Set s) {
		this.ie = ie;
		this.s = s;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return ie.toString() + " in " + s.toString();
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}

	public boolean evaluateBoolean(Substitution subs) {
		return false;//TODO
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(defaulted) return children;
		children.add(ie);
		children.add(s);
		return children;
	}

	public static SetOperator genSetOperator(int depth, ArrayList<Variable> variablesInScope) {
		IIntExpression ie = IntExpression.genIntExpression(depth, variablesInScope, true);
		Set s = Set.genSet(depth, variablesInScope);
		return new SetOperator(ie, s);
	}
}