package savile;
import java.util.ArrayList;
public class TableConstraint extends BooleanExpression {

	MatrixExpression variablesMatrix;
	MatrixExpression satistfyingTuples;

	public TableConstraint(MatrixExpression vars, MatrixExpression tups) {
		variablesMatrix = vars;
		satistfyingTuples = tups;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "table( " + variablesMatrix.toString() + ", " + satistfyingTuples.toString() + " )";
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}

	public boolean evaluateBoolean(Substitution subs) {
		return false;//TODO
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children =  new ArrayList<Token>();
		if(defaulted) return children;
		children.add(variablesMatrix);
		children.add(satistfyingTuples);
		return children;
	}

	public static TableConstraint genTableConstraint(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		MatrixExpression vars = genVariableMatrix(depth+1, variablesInScope, nonDecision);
		MatrixExpression tups = genTuples(depth+1, variablesInScope);
		return new TableConstraint(vars, tups);
	}

	public static MatrixExpression genVariableMatrix(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		//for now just create a normal matrix, 
		//but this is encapsulated to allow for future addition of a proper variable matrix
		return MatrixExpression.genMatrixExpression(depth, variablesInScope, nonDecision, false);//preventing multidimensional matrixes using false
	}

	public static MatrixExpression genTuples(int depth, ArrayList<Variable> variablesInScope) {
		//for now just create a normal matrix, 
		//but this is encapsulated to allow for future addition of a proper tuples matrix
		return MatrixExpression.genMatrixExpression(depth, variablesInScope, false, true);//nonDecision is implicit and multidimensional is set to true
	}
}