package savile;
import java.util.ArrayList;
public class NumericalOperator extends BooleanExpression {

	//numerical comparision operators
	public static final int EQUAL = 0;
	public static final int NOTEQUAL = 1;
	public static final int GREATER = 2;
	public static final int LESS = 3;
	public static final int GREATEROREQUAL = 4;
	public static final int LESSOREQUAL = 5;
	public static final int[] TYPES = new int[] {EQUAL,NOTEQUAL,GREATER,LESS,GREATEROREQUAL,LESSOREQUAL};

	public IIntExpression left;
	public IIntExpression right;
	public int operator;

	public NumericalOperator(int o, IIntExpression l, IIntExpression r) {
		left = l;
		right = r;
		operator = 0;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "(" + left.toString() + numericalOperatorToString() + right.toString() + ")";
	}

	public String numericalOperatorToString() {
		switch(operator) {
			case EQUAL:
				return "=";
			case NOTEQUAL:
				return "!=";
			case GREATER:
				return ">";
			case LESS:
				return "<";
			case GREATEROREQUAL:
				return ">=";
			case LESSOREQUAL:
				return "<=";
			default:
				return "{ERROR_INVALID_NUMERICAL_BOOLEAN_OPERATOR}";//TODO catch this error
		}
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}
	
	public boolean evaluateBoolean(Substitution subs) {
		IntEvaluation leftI = left.evaluateInt(subs);
		IntEvaluation rightI = right.evaluateInt(subs);
		return applyNumericalComparisionOperator(leftI, rightI);
	}

	private boolean applyNumericalComparisionOperator(IntEvaluation left, IntEvaluation right) {
		if(left.undefined || right.undefined) return false;
		int a = left.value;
		int b = right.value;
		switch(operator) {
			case EQUAL:
				return a == b;
			case NOTEQUAL:
				return a != b;
			case GREATER:
				return a > b;
			case LESS:
				return a < b;
			case GREATEROREQUAL:
				return a >= b;
			case LESSOREQUAL:
				return a <= b;
			default:
				return false;//TODO catch this error
		}
	}

	public ArrayList<Token> children() {
		ArrayList<Token> kids = new ArrayList<Token>();
		if(defaulted) return kids;
		kids.add(right);
		kids.add(left);
		return kids;
	}

	public static NumericalOperator genNumericalOperator(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		return new NumericalOperator(randOperator(), IntExpression.genIntExpression(depth+1, variablesInScope, nonDecision), IntExpression.genIntExpression(depth+1, variablesInScope, nonDecision));
	}

	private static int randOperator() {
		return TYPES[r.nextInt(TYPES.length)];
	}
}