package savile;
import java.util.ArrayList;
public class BoolVariable extends BooleanExpression {

	Variable v;

	public BoolVariable(Variable v) {
		this.v = v;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return v.name;
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}

	public boolean evaluateBoolean(Substitution subs) {
		Expression e = v.evaluateToExpression(subs);
		if(e!=null) return e.evaluateBoolean(subs);
		return false;
	}

	public ArrayList<Token> children() {
		return new ArrayList<Token>();
	}

	public static BoolVariable genBoolVariable(ArrayList<Variable> variablesInScope, boolean nonDecision) {
		ArrayList<Variable> boolsInScope = filterToBoolVariables(variablesInScope);
		if(nonDecision) boolsInScope = filterDecisions(boolsInScope);
		Variable v = boolsInScope.get(r.nextInt(boolsInScope.size()));
		return new BoolVariable(v);
	}
}