package savile;
import java.util.ArrayList;
public class GlobalConstraint extends BooleanExpression {
	//global constraints
	public static final int ALLDIFF = 1;
	public static final int ALLDIFFEXCEPT = 2;
	public static final int ATLEAST = 3;
	public static final int ATMOST = 4;
	public static final int GCC = 5;
	public static final int[] GLOBALCONSTRAINTS = new int[] {ALLDIFF,ALLDIFFEXCEPT,ATLEAST,ATMOST,GCC};

	int globalConstraint;
	MatrixExpression xMatrix;//for global constraints, denoted X in manual
	MatrixExpression cMatrix;//if the type is atleast, atmost or gcc denoted C in the manual. can have decision variables if type is GCC
	MatrixExpression valsMatrix;//if the type is atleast, atmost or gcc denoted Vals in the manual
	Expression exp;//if the type is ALLDIFFEXCEPT

	public GlobalConstraint(MatrixExpression exp) {
		globalConstraint = ALLDIFF;
		xMatrix = exp;
	}

	public GlobalConstraint(MatrixExpression mExp, Expression nonDecisionExp) {
		globalConstraint = ALLDIFFEXCEPT;
		xMatrix = mExp;
		exp = nonDecisionExp;
	}

	public GlobalConstraint(MatrixExpression xM, MatrixExpression cM, MatrixExpression valsM, int constraintType) {
		globalConstraint = constraintType;
		xMatrix = xM;
		cMatrix = cM;
		valsMatrix = valsM;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		switch(globalConstraint) {
			case ALLDIFF:
				return "allDiff(" + xMatrix.toString() + ")";
			case ALLDIFFEXCEPT:
				return "alldifferent_except(" + xMatrix.toString() + ", " + exp.toString() + ")";
			case ATLEAST:
				return "atleast(" + xMatrix.toString() + ", " + cMatrix.toString() + ", " + valsMatrix.toString() + ")";
			case ATMOST:
				return "atmost(" + xMatrix.toString() + ", " + cMatrix.toString() + ", " + valsMatrix.toString() + ")";
			case GCC:
				return "gcc(" + xMatrix.toString() + ", " + valsMatrix.toString() + ", " + cMatrix.toString() + ")";
			default:
				return "{ERROR_INVALID_GLOBALCONSTRAINT}";
		}
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}

	public boolean evaluateBoolean(Substitution subs) {
		return false;//TODO
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(defaulted) return children;
		children.add(xMatrix);
		if(globalConstraint==ALLDIFFEXCEPT) children.add(exp);
		if(globalConstraint==GCC || globalConstraint==ATMOST||globalConstraint==ATLEAST){
			children.add(cMatrix);
			children.add(valsMatrix);
		}
		return children;

	}

	public static GlobalConstraint genGlobalConstraint(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		int constraintType = randGlobalConstraintType();
		switch(constraintType) {
			case ALLDIFF:
				return new GlobalConstraint(MatrixExpression.genMatrixExpression(depth+1, variablesInScope, nonDecision, false));
			case ALLDIFFEXCEPT:
				return new GlobalConstraint(
					MatrixExpression.genMatrixExpression(depth+1, variablesInScope, nonDecision, false), 
					Expression.genExpression(depth+1, variablesInScope, true, Expression.genType()));
			case ATMOST:
			case ATLEAST:
			case GCC:
				return new GlobalConstraint(
					MatrixExpression.genMatrixExpression(depth+1, variablesInScope, nonDecision, false), 
					MatrixExpression.genMatrixExpression(depth+1, variablesInScope, ((constraintType==GCC)?nonDecision:true), false), 
					MatrixExpression.genMatrixExpression(depth+1, variablesInScope, true, false), 
					constraintType);
			default:
				System.out.println("genGlobalConstraint invalid constrainT type");
				return null;
		}
	}

	public static int randGlobalConstraintType() {
		return GLOBALCONSTRAINTS[r.nextInt(GLOBALCONSTRAINTS.length)];
	}
}