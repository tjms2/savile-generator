package savile;
import java.util.ArrayList;
public class BoolMatrixIndex extends BooleanExpression {

	public ArrayList<IIntExpression> indexes;
	public Variable v;

	public BoolMatrixIndex(Variable v, ArrayList<IIntExpression> is) {
		this.v = v;
		indexes = is;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return v.name + "[" + indexesToString() + "]";
	}

	public String indexesToString() {
		String str = "";
		for(int i = 0; i < indexes.size();i++) {
			if(i!=0)str+=",";
			IIntExpression ie = indexes.get(i);
			str += ie.toString();
		}
		return str;
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}
	
	public boolean evaluateBoolean(Substitution subs) {
		return false;//TODO
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(defaulted) return children;
		for(int i = 0; i < indexes.size(); i++) {
			children.add(indexes.get(i));
		}
		return children;
	}

	public static BoolMatrixIndex genBoolMatrixIndex(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		ArrayList<Variable> allMatrixes = filterToBoolMatrixVariables(variablesInScope);
		if(nonDecision) allMatrixes = filterDecisions(allMatrixes);
		Variable m = allMatrixes.get(r.nextInt(allMatrixes.size()));
		//generate index for matrix
		ArrayList<IIntExpression> indexes = new ArrayList<IIntExpression>();
		switch(m.category) {
			case Variable.PARAMETER:
			case Variable.QUANTIFIER:
			case Variable.DECISION:
				MatrixDomain matrixDomain = m.d.matrixDomain;
				for(int i = 0; i < matrixDomain.numDimensions; i++) {
					if(matrixDomain.explicit) {
						DomainExpression matrixDimension = matrixDomain.matrixDimensions.get(i);
						switch(matrixDimension.type) {
							case Expression.INTEGER: //TODO check for inbounds
								indexes.add(IntExpression.genIntExpression(depth+1, variablesInScope, true));
								break;
							case Expression.BOOLEAN:
								indexes.add(genBooleanExpression(depth+1, variablesInScope, true));
								break;
							default: return null;//catch this
						}
					} else {
						indexes.add(IntExpression.genIntExpression(depth+1, variablesInScope, true));
					}
				}
				break;
			case Variable.CONSTANT:
				int numDimensions = ((MatrixExpression) m.e).numDimensions();
				for(int i = 0; i < numDimensions; i++) {
					indexes.add(IntExpression.genIntExpression(depth+1, variablesInScope, true));
				}
				break;
			default:
				return null;//TODO catch this
		}
		return new BoolMatrixIndex(m, indexes);
	}
}