package savile;
import java.util.ArrayList;
public class MatrixComparisionOperator extends BooleanExpression {
	public static final int LEXGREATER = 1;
	public static final int LEXGREATEROREQUAL = 2;
	public static final int LEXLESS = 3;
	public static final int LEXLESSOREQUAL = 4;
	public static final int[] MATRIXCOMPARISIONTYPES = new int[] {LEXLESSOREQUAL,LEXLESS,LEXGREATEROREQUAL,LEXGREATER};

	int operator;
	MatrixExpression left;//should be one dimensional
	MatrixExpression right;//should be one dimensional

	public MatrixComparisionOperator(MatrixExpression left, MatrixExpression right, int type) {
		this.left = left;
		this.right = right;
		operator = type;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return left.toString() + operatorToString() + right.toString();
	}

	String operatorToString() {
		switch(operator) {
			case LEXGREATER:
			return " >lex ";
			case LEXGREATEROREQUAL:
			return " >=lex ";
			case LEXLESSOREQUAL:
			return " <lex ";
			case LEXLESS:
			return " <=lex ";
			default: 
			return "{ERROR_Matrix_Comparision_Invalid_Operator}";
		}
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}

	public boolean evaluateBoolean(Substitution subs) {
		return false;//TODO
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(defaulted) return children;
		children.add(left);
		children.add(right);
		return children;
	}

	public static MatrixComparisionOperator genMatrixComparisionOperator(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		MatrixExpression l = MatrixExpression.genMatrixExpression(depth+1, variablesInScope, nonDecision, false);
		MatrixExpression r = MatrixExpression.genMatrixExpression(depth+1, variablesInScope, nonDecision, false);
		return new MatrixComparisionOperator(l, r, randMatrixComparisionOperator());
	}

	public static int randMatrixComparisionOperator(){
		return MATRIXCOMPARISIONTYPES[r.nextInt(MATRIXCOMPARISIONTYPES.length)];
	}
}