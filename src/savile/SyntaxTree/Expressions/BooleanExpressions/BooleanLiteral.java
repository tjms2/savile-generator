package savile;
import java.util.ArrayList;
public class BooleanLiteral extends BooleanExpression {

	boolean val;

	public BooleanLiteral(boolean b) {
		this.val = b;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return val ? "true" : "false";
	}

	public String toDefaultString() {
		return DEFAULTBOOL;
	}

	public boolean evaluateBoolean(Substitution subs) {
		return val;
	}

	public ArrayList<Token> children() {
		return new ArrayList<Token>();
	}

	public static BooleanLiteral genBooleanLiteral() {
		return new BooleanLiteral(r.nextBoolean());
	}
}