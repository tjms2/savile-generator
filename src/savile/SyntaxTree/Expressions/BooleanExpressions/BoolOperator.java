package savile;
import java.util.ArrayList;
public class BoolOperator extends BooleanExpression {
	
	public static final int AND = 1;
	public static final int OR = 2;
	public static final int IMPLIES = 3;
	public static final int IFONLYIF = 4;
	public static final int[] TYPES = new int[] {AND,OR,IMPLIES,IFONLYIF};

	int operator;
	BooleanExpression left;
	BooleanExpression right;

	BoolOperator(int o, BooleanExpression l, BooleanExpression r) {
		operator = o;
		left = l;
		right = r;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "(" + left.toString() + operatorString() + right.toString() + ")";
	}

	public String operatorString() {
		switch(operator) {
			case AND :
				return "/\\";
			case OR :
				return "\\/";
			case IMPLIES :
				return "->";
			case IFONLYIF :
				return "<->";
			default :
				return "{ERROR_INVALID_BOOLEAN_OPERATOR_" + operator+"}";
		}
	}

	public String toDefaultString() {
		return right.toString();
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) children.add(left);
		children.add(right);
		return children;
	}

	public boolean evaluateBoolean(Substitution subs) {
		boolean leftVal = left.evaluateBoolean(subs);
		boolean rightVal = right.evaluateBoolean(subs);
		return applyOperator(leftVal, rightVal);
	}

	private boolean applyOperator(boolean a, boolean b) {
		switch(operator) {
			case AND :
				return a&&b;
			case OR :
				return a||b;
			case IMPLIES :
				return (!a)||b;
			case IFONLYIF :
				return a==b;
			default :
				return false;//TODO catch this
		}
	}

	public static BoolOperator genBoolOperator(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		BooleanExpression left = genBooleanExpression(depth+1, variablesInScope, nonDecision);
		BooleanExpression right = genBooleanExpression(depth+1, variablesInScope, nonDecision);
		return new BoolOperator(randBoolOperator(), left, right);
	}

	public static int randBoolOperator() {
		return TYPES[r.nextInt(TYPES.length)];
	}
}