package savile;
import java.util.ArrayList;
public abstract class BooleanExpression extends IIntExpression{

	public static final int LITERAL = 0;
	public static final int VARIABLE = 1;
	public static final int OPERATOR = 3;
	public static final int NEGATION = 4;
	public static final int NUMERICALCOMPARISION = 5;
	public static final int MATRIXINDEX = 6;
	public static final int QUANTIFIER = 8;
	public static final int GLOBALCONSTRAINT = 10;
	public static final int MATRIXCOMPARISION = 11;
	public static final int TABLECONSTRAINT = 12;
	public static final int SETOPERATOR = 13;
	public static final int[] BOOLEANEXPRESSIONTYPES = new int[] {LITERAL,VARIABLE,OPERATOR,NEGATION,NUMERICALCOMPARISION,MATRIXINDEX,QUANTIFIER,GLOBALCONSTRAINT,MATRIXCOMPARISION,TABLECONSTRAINT,SETOPERATOR};
	public static final String DEFAULTBOOL = "true";

	public abstract boolean evaluateBoolean(Substitution subs);
	
	public int expressionType() {
		return BOOLEAN;
	}

	public IntEvaluation evaluateInt(Substitution subs) {
		return new IntEvaluation(evaluateBoolean(subs) ? 1 : 0);
	}

	public static BooleanExpression genBooleanExpression(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		switch(genValidBoolExpressionType(depth,variablesInScope,nonDecision)) {
			case LITERAL:
				return BooleanLiteral.genBooleanLiteral();
			case VARIABLE:
				return BoolVariable.genBoolVariable(variablesInScope, nonDecision);
			case OPERATOR:
				return BoolOperator.genBoolOperator(depth, variablesInScope, nonDecision);
			case NEGATION:
				return Negation.genNegation(depth, variablesInScope, nonDecision);
			case NUMERICALCOMPARISION:
				return NumericalOperator.genNumericalOperator(depth, variablesInScope, nonDecision);
			case MATRIXINDEX:
				return BoolMatrixIndex.genBoolMatrixIndex(depth, variablesInScope, nonDecision);
			case QUANTIFIER:
				return Quantifier.genQuantifier(depth, variablesInScope, nonDecision);
			case GLOBALCONSTRAINT:
				return GlobalConstraint.genGlobalConstraint(depth, variablesInScope, nonDecision);
			case MATRIXCOMPARISION:
				return MatrixComparisionOperator.genMatrixComparisionOperator(depth, variablesInScope, nonDecision);
			case TABLECONSTRAINT:
				return TableConstraint.genTableConstraint(depth, variablesInScope, nonDecision);
			case SETOPERATOR:
				return SetOperator.genSetOperator(depth, variablesInScope);
			default: 
				System.out.println("genBoolExpression invalid expression type");
				return null;
		}
	}


	public static int genValidBoolExpressionType(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		int type = randBoolExpressionType();
		if(depth>=Configurations.RECURSION_DEPTH) {
			while(true){
					//must be atomic
					if(type == LITERAL) return type;
					if(type == VARIABLE) {
						ArrayList<Variable> allVars = filterToBoolVariables(variablesInScope);
						if(nonDecision) allVars = filterDecisions(allVars);
						if(allVars.size()>=1) return type;
					}	
					type = randBoolExpressionType();				
			}
		} else {
			if(r.nextBoolean())return OPERATOR;
			while(true){
				if(type==VARIABLE){
						ArrayList<Variable> allVars = filterToBoolVariables(variablesInScope);
						if(nonDecision) allVars = filterDecisions(allVars);
						if(allVars.size()>=1) return type;
				} else {
					if(type ==MATRIXINDEX) {
						//check if there are any valid matrixes to index
						ArrayList<Variable> allVars = filterToBoolMatrixVariables(variablesInScope);
						if(nonDecision) allVars = filterDecisions(allVars);
						if(allVars.size()>=1) return type;
					} else {
						return type;
					}
				}
				type = randBoolExpressionType();
			}
		}
	}

	public static int randBoolExpressionType() {
		return BOOLEANEXPRESSIONTYPES[r.nextInt(BOOLEANEXPRESSIONTYPES.length)];		
	}

	protected static ArrayList<Variable> filterToBoolVariables(ArrayList<Variable> vars) {
		ArrayList<Variable> newList = new ArrayList<Variable>();
		for(int i = 0;i<vars.size();i++) {
			Variable v = vars.get(i);
			if(v.type == Expression.BOOLEAN) newList.add(v);
		}
		return newList;
	}

	protected static ArrayList<Variable> filterToBoolMatrixVariables(ArrayList<Variable> vars) {
		ArrayList<Variable> newList = new ArrayList<Variable>();
		for(int i = 0;i<vars.size();i++) {
			Variable v = vars.get(i);
			if(v.type == Expression.MATRIX) {
				int baseType = -1;
				if(v.category==Variable.CONSTANT) {
					baseType = ((MatrixExpression) v.e).baseType();
				} else {
					baseType = v.d.matrixDomain.baseType;
				}
				if(baseType == Expression.BOOLEAN)
					newList.add(v);			
			}
		}
		return newList;
	}
}