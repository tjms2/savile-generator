package savile;
import java.util.ArrayList;
public class Negation extends BooleanExpression {
	BooleanExpression exp;

	Negation(BooleanExpression b) {
		exp = b;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "!(" + exp.toString() + ")";
	}

	public String toDefaultString() {
		return exp.toString();
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		children.add(exp);
		return children;
	}

	public boolean evaluateBoolean(Substitution subs) {
		boolean expVal = exp.evaluateBoolean(subs);
		return !expVal;
	}

	public static Negation genNegation(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision) {
		BooleanExpression b = genBooleanExpression(depth+1, variablesInScope, nonDecision);
		return new Negation(b);
	}
}