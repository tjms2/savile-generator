package savile;
public abstract class IIntExpression extends Expression {
	public abstract IntEvaluation evaluateInt(Substitution subs);
}