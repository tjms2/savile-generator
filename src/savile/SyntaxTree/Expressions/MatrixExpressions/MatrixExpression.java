package savile;
import java.util.ArrayList;
public abstract class MatrixExpression extends Expression {
	//literal
	//variable
	//expression

	//types of matrix expression
	public static final int LITERAL = 0;
	public static final int VARIABLE = 1;
	public static final int SLICE = 2;
	public static final int COMPREHENSION = 3;
	public static final int FLATTENED = 4;
	public static final int[] TYPES = new int[]{LITERAL,VARIABLE,SLICE,COMPREHENSION,FLATTENED};
	public static final String DEFAULTMATRIX = "[true]";

	public int matrixType;//type of value in the matrix (e.g. other matrixexpressions, intexpressions, or booleanexpressions)

	public abstract int baseType();
	public abstract int numDimensions();
	public abstract MatrixEvaluation evaluateMatrix(Substitution subs);

	public int expressionType(){
		return Expression.MATRIX;
	}

	public boolean evaluateBoolean(Substitution subs) {
		return false;
	}

	public IntEvaluation evaluateInt(Substitution subs) {
		return null;
	}

	public static MatrixExpression genMatrixExpression(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, boolean multiDimensional) {
			switch(genValidMatrixExpressionType(depth, variablesInScope, nonDecision, multiDimensional)) {//TODO switch with valid matrix type checker
				case LITERAL :
					return MatrixLiteral.genMatrixLiteral(depth, variablesInScope, nonDecision, multiDimensional);
				case VARIABLE :
					return MatrixVariable.genMatrixVariable(depth, variablesInScope, nonDecision, multiDimensional);
				case SLICE :
					return MatrixSlice.genMatrixSlice(depth, variablesInScope, nonDecision, multiDimensional);
				case COMPREHENSION :
					return MatrixComprehension.genMatrixComprehension(depth, variablesInScope, nonDecision, multiDimensional);
				case FLATTENED:
					return FlattenedMatrix.genFlattenedMatrix(depth, variablesInScope, nonDecision, multiDimensional);
				default :
					return null;//catch this
			}
	}

	public static int genValidMatrixExpressionType(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, boolean multiDimensional) {
		int type = randMatrixExpressionType();
		if(depth>=Configurations.RECURSION_DEPTH) {
			while(true){
					//must be atomic
					if(type == MatrixExpression.LITERAL) return type;
					if(type == MatrixExpression.VARIABLE) {
						if(multiDimensional) {
							ArrayList<Variable> allVars = filterToMatrixVariables(variablesInScope);
							if(nonDecision) allVars = filterDecisions(allVars);
							if(allVars.size()>=1)
								return type;
						} else {
							ArrayList<Variable> allVars = filterToOneDimensionalMatrixVariables(variablesInScope);
							if(nonDecision) allVars = filterDecisions(allVars);
							if(allVars.size()>=1)
								return type;
						}
						
					}	
					type = randMatrixExpressionType();				
			}
		} else {
			while(true){
				if(type == MatrixExpression.VARIABLE || type == MatrixExpression.SLICE){
						if(multiDimensional) {
							ArrayList<Variable> allVars = filterToMatrixVariables(variablesInScope);
							if(nonDecision) allVars = filterDecisions(allVars);
							if(allVars.size()>=1)
								return type;
						} else {
							ArrayList<Variable> allVars = filterToOneDimensionalMatrixVariables(variablesInScope);
							if(nonDecision) allVars = filterDecisions(allVars);
							if(allVars.size()>=1)
								return type;
						}
				} else {
					return type;
				}
				type = randMatrixExpressionType();
			}
		}
	}

	public static int randMatrixExpressionType() {
		return TYPES[r.nextInt(TYPES.length)];		
	}

	protected static ArrayList<Variable> filterToMatrixVariables(ArrayList<Variable> vars) {
		ArrayList<Variable> newList = new ArrayList<Variable>();
		for(int i = 0;i<vars.size();i++) {
			Variable v = vars.get(i);
			if(v.type == Expression.MATRIX) 
				newList.add(v);					
		}
		return newList;
	}

	protected static ArrayList<Variable> filterToOneDimensionalMatrixVariables(ArrayList<Variable> vars) {
		ArrayList<Variable> newList = new ArrayList<Variable>();
		for(int i = 0;i<vars.size();i++) {
			Variable v = vars.get(i);
			if(v.type == Expression.MATRIX) {
				int dimensions = 0;
				if(v.category==Variable.CONSTANT) {
					dimensions = ((MatrixExpression) v.e).numDimensions();
				} else {
					dimensions = v.d.matrixDomain.numDimensions;
				}
				if(dimensions == 1)
					newList.add(v);				
			}
		}
		return newList;
	}
}
