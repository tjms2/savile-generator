package savile;
import java.util.ArrayList;
import java.util.Arrays;
public class MatrixLiteral extends MatrixExpression {

	public ArrayList<Expression> literalContents = new ArrayList<Expression>();
	public DomainExpression d;

	public MatrixLiteral(ArrayList<Expression> exps, int matrixType, DomainExpression d) {
		literalContents = exps;
		this.matrixType = matrixType;
		this.d = d;
	}

	public int baseType() {
		if(matrixType!=Expression.MATRIX) return matrixType;
		if(literalContents.size() > 0) {
			MatrixExpression child =  (MatrixExpression) literalContents.get(0);
			return child.baseType();
		} else {
			return 0;
		}
	}

	public int numDimensions() {
		if(matrixType != Expression.MATRIX) return 1;
		if(literalContents.size() == 0) return 0;
		MatrixExpression child =  (MatrixExpression) literalContents.get(0);
		return child.numDimensions()+1;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "[" + literalContentsToString() + "]";
	}

	public String literalContentsToString() {
		String str = "";
		for(int i = 0; i < literalContents.size(); i ++){
			if(i!=0) str+=",";
			str += literalContents.get(i).toString();
		}
		return str;
	}

	public String toDefaultString() {
		if(literalContents.size() >0)
			return "[" + literalContents.get(0).toString() + "]";
		return DEFAULTMATRIX;
	}

	public MatrixEvaluation evaluateMatrix(Substitution subs) {
		return null;
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			for(int i = 0; i < literalContents.size(); i++) {
				children.add(literalContents.get(i));
			}
		} else {
			if(literalContents.size()>0) children.add(literalContents.get(0));
		}
		return children;
	}

	public static MatrixLiteral genMatrixLiteral(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, boolean multiDimensional) {
		int[] indexes = new int[multiDimensional ? r.nextInt(Configurations.RECURSION_DEPTH)+1 : 1];//the dimensions of the matrix and sub matrixes
		for(int i =0;i<indexes.length;i++) {
			indexes[i] = r.nextInt(Configurations.MAX_REPETITIONS)+1;
		}
		int baseType = r.nextBoolean() ? Expression.BOOLEAN : Expression.INTEGER;//the base type of the matrix
		return genmatrixLiteral(depth, 0, variablesInScope, nonDecision, baseType, indexes);
	}

	public static MatrixLiteral genmatrixLiteral(int depth, int matrixDepth, ArrayList<Variable> variablesInScope, boolean nonDecision, int baseType, int[] indexes) {
		//make matrix literal
		ArrayList<Expression> expressions = new ArrayList<Expression>();
		int matrixType = matrixDepth == indexes.length-1 ? baseType : Expression.MATRIX;
		for(int i = 0; i < indexes[matrixDepth]; i++) {
			if(matrixDepth == indexes.length-1) {
				//base case where we need to fill the matrix with values of the base type
				expressions.add(Expression.genExpression(depth+1, variablesInScope, nonDecision, baseType));
			} else {
				//we need to fill the matrix with sub matrixes of equal domains (using indexes)
				expressions.add(genmatrixLiteral(depth+1, matrixDepth+1, variablesInScope, nonDecision, baseType, indexes));
			}
		}
		int[] subDomainAsArray = Arrays.copyOfRange(indexes, matrixDepth, indexes.length);
		return new MatrixLiteral(expressions, matrixType, MatrixDomain.genAutoMatrixDomain(subDomainAsArray, baseType));
	}
}