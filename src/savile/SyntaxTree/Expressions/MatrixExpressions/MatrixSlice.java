package savile;
import java.util.ArrayList;
public class MatrixSlice extends MatrixExpression {

	Variable v;
	ArrayList<MatrixIndex> sliceIndex;

	public MatrixSlice(Variable v, ArrayList<MatrixIndex> indexes) {
		this.v = v;
		switch(v.category) {
			case Variable.DECISION :
			case Variable.PARAMETER :
			case Variable.QUANTIFIER :
				this.matrixType = v.d.matrixDomain.numDimensions > 1 ? Expression.MATRIX : v.d.matrixDomain.baseType;
				break;
			case Variable.CONSTANT :
				this.matrixType = ((MatrixExpression)v.e).matrixType;
				break;
		}
		sliceIndex = indexes;
	}

	public int baseType() {
		if(matrixType != Expression.MATRIX) return matrixType;
		switch(v.category) {
			case Variable.DECISION :
			case Variable.PARAMETER :
			case Variable.QUANTIFIER :
				return v.d.matrixDomain.baseType;
			case Variable.CONSTANT :
				return ((MatrixExpression) v.e).baseType();
			default :
				return 0;//TODO catch this!
		}
	}

	public int numDimensions() {
		if(matrixType != Expression.MATRIX) return 1;//TODO check this
		int numdefinedIndexes = 0;
		for(int i =0;i< sliceIndex.size(); i++) {
			if(sliceIndex.get(i).defined) numdefinedIndexes++;
		}
		switch(v.category) {
			case Variable.DECISION :
			case Variable.PARAMETER :
			case Variable.QUANTIFIER :
				return v.d.matrixDomain.numDimensions - numdefinedIndexes;
			case Variable.CONSTANT :
				return ((MatrixExpression) v.e).numDimensions() - numdefinedIndexes;
			default :
				return 0;
		}
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return v.name +"[" + indexToString() + "]";
	}

	public String indexToString() {
		String str = "";
		for(int i = 0; i < sliceIndex.size(); i ++){
			if(i!=0) str+=",";
			str += sliceIndex.get(i).toString();
		}
		return str;
	}

	public String toDefaultString() {
		return DEFAULTMATRIX;
	}

	public MatrixEvaluation evaluateMatrix(Substitution subs) {
		return null;
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			for(int i = 0; i < sliceIndex.size(); i++) {
				children.add(sliceIndex.get(i));
			}
		}
		return children;
	}

	public static MatrixSlice genMatrixSlice(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, boolean multiDimensional) {
		ArrayList<Variable> allMatrixes;
		if(multiDimensional) {
			allMatrixes = filterToMatrixVariables(variablesInScope);
			if(nonDecision) allMatrixes = filterDecisions(allMatrixes);
		} else {
			allMatrixes = filterToOneDimensionalMatrixVariables(variablesInScope);
			if(nonDecision) allMatrixes = filterDecisions(allMatrixes);
		}
		Variable m = allMatrixes.get(r.nextInt(allMatrixes.size()));
		ArrayList<MatrixIndex> indexes = new ArrayList<MatrixIndex>();
		//generate index for matrix
		switch(m.category) {
			case Variable.PARAMETER:
			case Variable.QUANTIFIER:
			case Variable.DECISION:
				MatrixDomain matrixDomain = m.d.matrixDomain;
				//create list of dimensions to slice, of length numDimensionsToSlice
				boolean[] dimensionsToSlice = getDimensionsToSlice(matrixDomain.numDimensions, multiDimensional);
				for(int i = 0; i < matrixDomain.numDimensions; i++) {
					if(matrixDomain.explicit) {
						DomainExpression matrixDimension = matrixDomain.matrixDimensions.get(i);
						switch(matrixDimension.type) {
							case Expression.INTEGER: //TODO check for inbounds
								MatrixIndex index = new MatrixIndex(IntExpression.genIntExpression(depth+1, variablesInScope, true));
								if(dimensionsToSlice[i]){
									index = new MatrixIndex();
								}
								indexes.add(index);
								break;
							case Expression.BOOLEAN:
								MatrixIndex index_ = new MatrixIndex(BooleanExpression.genBooleanExpression(depth+1, variablesInScope, true));
								if(dimensionsToSlice[i]){
									index_ = new MatrixIndex();
								}
								indexes.add(index_);
								break;
							default: return null;//catch this
						}
					} else {
						MatrixIndex index = new MatrixIndex(IntExpression.genIntExpression(depth+1, variablesInScope, true));
						if(dimensionsToSlice[i]){
							index = new MatrixIndex();
						}
						indexes.add(index);
					}
					
				}
				break;
			case Variable.CONSTANT:
				int numDimensions = ((MatrixExpression) m.e).numDimensions();
				dimensionsToSlice = getDimensionsToSlice(numDimensions, multiDimensional);
				for(int i = 0; i < numDimensions; i++) {
					MatrixIndex index = new MatrixIndex(IntExpression.genIntExpression(depth+1, variablesInScope, true));
					if(dimensionsToSlice[i]){
						index = new MatrixIndex();
					}	
					indexes.add(index);
				}
				break;
			default:
				return null;//TODO catch this
		}
		return new MatrixSlice(m, indexes);	
	}

	//returns an array of booleans with length equal to numDimensions.
	//each value is either true, meaning the dimension should be sliced, or false meaning the dimension shouldn't be.
	public static boolean[] getDimensionsToSlice(int numDimensions, boolean multiDimensional) {
		if(numDimensions<=0) return new boolean[0];//handles undefined sub matrixes;
		int numDimensionsToSlice = 1;	//returns 1 if one-dimensional else 1..numDimensions-1
		if(multiDimensional && numDimensions > 1) 
				numDimensionsToSlice = r.nextInt(numDimensions-1)+1;
		boolean[] dimensionsToSlice = new boolean[numDimensions];
		for(int i=0;i<numDimensionsToSlice;i++) {
			while(true){
				int dimension = r.nextInt(numDimensions);
				if(!dimensionsToSlice[dimension]) {
					dimensionsToSlice[dimension] = true;
					break;
				}
			}
		}
		return dimensionsToSlice;
	}
}