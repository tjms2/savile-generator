package savile;
import java.util.ArrayList;
public class MatrixVariable extends MatrixExpression {

	Variable v;
	DomainExpression d;
	

	public MatrixVariable(Variable v) {
		this.v = v;
		this.d = v.d;
		switch(v.category) {
			case Variable.DECISION :
			case Variable.PARAMETER :
			case Variable.QUANTIFIER :
				this.matrixType = v.d.matrixDomain.numDimensions > 1 ? Expression.MATRIX : v.d.matrixDomain.baseType;
				break;
			case Variable.CONSTANT :
				this.matrixType = ((MatrixExpression)v.e).matrixType;
				break;
		}
	}

	public int baseType() {
		if(matrixType != Expression.MATRIX) return matrixType;
		switch(v.category) {
			case Variable.DECISION :
			case Variable.PARAMETER :
			case Variable.QUANTIFIER :
				return v.d.matrixDomain.baseType;
			case Variable.CONSTANT :
				return ((MatrixExpression) v.e).baseType();
			default :
				return 0;//TODO catch this!
		}
	}

	public int numDimensions() {
		if(matrixType != Expression.MATRIX) return 1;
		switch(v.category) {
			case Variable.DECISION :
			case Variable.PARAMETER :
			case Variable.QUANTIFIER :
				return v.d.matrixDomain.numDimensions;
			case Variable.CONSTANT :
				return ((MatrixExpression) v.e).numDimensions();
			default :
				return 0;//TODO catch this!
		}
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return v.name;
	}
	public String toDefaultString() {
		return DEFAULTMATRIX;
	}

	public MatrixEvaluation evaluateMatrix(Substitution subs) {
		Expression e = v.evaluateToExpression(subs);
		if(e!=null) return e.evaluateMatrix(subs);
		return new MatrixEvaluation();
	}

	public ArrayList<Token> children() {
		return new ArrayList<Token>();
	}

	public static MatrixVariable genMatrixVariable(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, boolean multiDimensional) {
		if(multiDimensional) {
			ArrayList<Variable> allVars = filterToMatrixVariables(variablesInScope);
			if(nonDecision) allVars = filterDecisions(allVars);
			return new MatrixVariable(allVars.get(r.nextInt(allVars.size())));
		} else {
			ArrayList<Variable> allVars = filterToOneDimensionalMatrixVariables(variablesInScope);
			if(nonDecision) allVars = filterDecisions(allVars);
			return new MatrixVariable(allVars.get(r.nextInt(allVars.size())));
		}
	}
}