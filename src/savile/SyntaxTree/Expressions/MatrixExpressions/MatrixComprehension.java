package savile;
import java.util.ArrayList;
public class MatrixComprehension extends MatrixExpression {

	public Expression comprehensionExpression;
	public ArrayList<Variable> comprehensionVariables;
	public ArrayList<BooleanExpression> comprehensionConstraints;

	public MatrixComprehension(Expression exp, ArrayList<Variable> comprehensions, ArrayList<BooleanExpression> constraints) {
		comprehensionExpression = exp;
		comprehensionVariables = comprehensions;
		comprehensionConstraints = constraints;
		this.matrixType = exp.expressionType();
	}

	public int baseType() {
		if(matrixType!=Expression.MATRIX) return matrixType;
		return 1 + ((MatrixExpression) comprehensionExpression).baseType();
	}

	public int numDimensions() {
		if(matrixType != Expression.MATRIX) return 1;
		return 1 + ((MatrixExpression) comprehensionExpression).numDimensions();
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "[ " + comprehensionExpression.toString() + " | " + comprehensionVarsAndConstraintsToString() + " ]";
	}

	public String comprehensionVarsAndConstraintsToString() {
		int count = 0;
		String str = "";
		for(int i = 0; i < comprehensionVariables.size(); i++) {
			if(count != 0) str += ", ";
			str += comprehensionVariables.get(i).name + " : " + comprehensionVariables.get(i).d.toString();
			count ++;
		}
		for(int i = 0; i < comprehensionConstraints.size(); i++) {
			if(count != 0) str += ", ";
			str += comprehensionConstraints.get(i).toString();
			count ++;
		}
		return str;
	}

	public String toDefaultString() {
		return DEFAULTMATRIX;
	}

	public MatrixEvaluation evaluateMatrix(Substitution subs) {
		return null;
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			children.add(comprehensionExpression);
			for(int i = 0; i < comprehensionVariables.size(); i++) {
				children.add(comprehensionVariables.get(i).d);
			}
			for(int i = 0; i < comprehensionConstraints.size(); i++) {
				children.add(comprehensionConstraints.get(i));
			}
		}
		return children;
	}

	public static MatrixExpression genMatrixComprehension(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, boolean multiDimensional) {
		//create some copmrehension variables
		ArrayList<Variable> quants = variablesInScope;
		ArrayList<Variable> comprehensionVariables = new ArrayList<Variable>();
		for(int i = -1; i < r.nextInt(Configurations.MAX_REPETITIONS); i ++ ) {
			Variable quant = Variable.genQuantifierVariable(quants, depth+1);
			quants = copyAdd(quants, quant);
			comprehensionVariables.add(quant);
		}
		//possibly create some constraints using the comprehensionVariables
		ArrayList<BooleanExpression> constraints = new ArrayList<BooleanExpression>();
		for(int i = 0; i < r.nextInt(Configurations.MAX_REPETITIONS); i ++ ) {
			BooleanExpression constraint = BooleanExpression.genBooleanExpression(depth+1, quants, nonDecision);
			constraints.add(constraint);
		}
		//create the expression
		Expression exp;
		if(multiDimensional) {
			exp = Expression.genExpression(depth+1, quants, nonDecision, Expression.genType());
		} else {
			exp = Expression.genExpression(depth+1, quants, nonDecision, r.nextBoolean() ? Expression.BOOLEAN : Expression.INTEGER);
		}
		return new MatrixComprehension(exp, comprehensionVariables, constraints);
	}
}