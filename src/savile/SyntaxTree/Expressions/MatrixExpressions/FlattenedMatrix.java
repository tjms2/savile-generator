package savile;
import java.util.ArrayList;
public class FlattenedMatrix extends MatrixExpression {

	MatrixExpression m;
	IIntExpression n;
	boolean numDimensionsToFlatten;

	public FlattenedMatrix(MatrixExpression m) {
		numDimensionsToFlatten = false;
		this.m = m;
		matrixType = m.baseType();
	}

	public FlattenedMatrix(MatrixExpression m, IIntExpression n) {
		numDimensionsToFlatten = true;
		this.m = m;
		this.n = n;
		matrixType = (numDimensions() > 1) ? Expression.MATRIX : m.baseType();//messy
	}


	public int baseType() {
		if(matrixType!=Expression.MATRIX) return matrixType;
		return m.baseType();
	}

	public int numDimensions() {
		if(!numDimensionsToFlatten) return 1;
		int unflattenedNumber = m.numDimensions();
		IntEvaluation nEvaluated = n.evaluateInt(new Substitution(new ArrayList<Variable>()));//super hacky and needs to be checked
		if(nEvaluated.undefined) return 0;//undefined matrix expression
		return unflattenedNumber - nEvaluated.value;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "flatten(" + nString() + m.toString() + ")";
	}

	public String nString() {
		if(!numDimensionsToFlatten) return "";
		return n.toString() + ", ";
	}

	public String toDefaultString() {
		return m.toString();
	}

	public MatrixEvaluation evaluateMatrix(Substitution subs) {
		return null;
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		children.add(m);
		if((!defaulted) && numDimensionsToFlatten) children.add(n);
		return children;
	}

	public static FlattenedMatrix genFlattenedMatrix(int depth, ArrayList<Variable> variablesInScope, boolean nonDecision, boolean multiDimensional) {
		MatrixExpression unflattenedM = MatrixExpression.genMatrixExpression(depth+1, variablesInScope, nonDecision, true);
		if(r.nextBoolean() && multiDimensional) {
			IIntExpression n = IntExpression.genIntExpression(depth+1, variablesInScope, nonDecision);
			return new FlattenedMatrix(unflattenedM, n);
		}
		return new FlattenedMatrix(unflattenedM);
	}
}