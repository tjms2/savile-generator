package savile;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.Arrays;

public class Model extends Token {
	private String header = "language ESSENCE' 1.0";

	public ArrayList<Variable> variables  = new ArrayList<Variable>();

	public ArrayList<Variable> constants = new ArrayList<Variable>();
	public ArrayList<Variable> parameterVariables = new ArrayList<Variable>();
	public ArrayList<Variable> decisionVariables = new ArrayList<Variable>();
	public ArrayList<Variable> notDecisionVariables = new ArrayList<Variable>();

	public ArrayList<Variable> boolVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intAndBoolVariables = new ArrayList<Variable>();
	public ArrayList<Variable> matrixVariables = new ArrayList<Variable>();

	public ArrayList<Variable> boolNotDecisionVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intNotDecisionVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intAndBoolNotDecisionVariables = new ArrayList<Variable>();
	public ArrayList<Variable> matrixNotDecisionVariables = new ArrayList<Variable>();

	public ArrayList<Variable> booleanMatrixVariables = new ArrayList<Variable>();
	public ArrayList<Variable> booleanMatrixNotDecisionVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intMatrixVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intMatrixNotDecisionVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intAndBoolMatrixVariables = new ArrayList<Variable>();
	public ArrayList<Variable> intAndBoolNotDecisionMatrixVariables = new ArrayList<Variable>();
	public ArrayList<Variable> oneDimensionalMatrixVariables = new ArrayList<Variable>();
	public ArrayList<Variable> oneDimensionalNotDecisionMatrixVariables = new ArrayList<Variable>();

	public ArrayList<VariableDeclaration> constantDefinitions = new ArrayList<VariableDeclaration>();
	public ArrayList<VariableDeclaration> parameterDeclarations = new ArrayList<VariableDeclaration>();
	public ArrayList<VariableDeclaration> decisionDeclarations = new ArrayList<VariableDeclaration>();

	public Objective objective;
	public Constraints constraints;

	public void printDecisionVariableDomains() {//for testing
		System.out.println();
		for(int i = 0; i < decisionVariables.size(); i++) {
			System.out.println("\n" + decisionVariables.get(i).name + "\t: " + decisionVariables.get(i).d.domainToSetString());
		}
	}


	public void print() {
		System.out.println();
		System.out.println(toString());
		System.out.println();
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		String theModel = header;
		for(int i =0;i<constantDefinitions.size();i++){
			theModel += "\n" + constantDefinitions.get(i).toString();
		}
		for(int i =0;i<parameterDeclarations.size();i++){
			theModel += "\n" + parameterDeclarations.get(i).toString();
		}
		for(int i =0;i<decisionDeclarations.size();i++){
			theModel += "\n" + decisionDeclarations.get(i).toString();
		}

		if(objective!=null)theModel += "\n" + objective.toString();
		theModel += "\n" + constraints.toString();
		String[] theModelSplit = theModel.split("\n");
		String newModel = "";
		for(int i = 0; i < theModelSplit.length; i++) {
			if(!theModelSplit[i].equals("")){
				newModel += theModelSplit[i];
			}
		}
		return theModel;
	}

	public String toDefaultString() {
		return header;
	}
	
	public void addVariableDeclaration(VariableDeclaration vd) {
		switch(vd.category) {
			case Variable.PARAMETER :
				addVariable(vd.v);
				parameterDeclarations.add(vd);
				break;
			case Variable.DECISION :
				for(int i =0;i<vd.decisions.size();i++) {
					addVariable(vd.decisions.get(i));
				}
				decisionDeclarations.add(vd);
				break;
			case Variable.CONSTANT :
				addVariable(vd.v);
				constantDefinitions.add(vd);
				break;
		}
	}

	public void addVariable(Variable v) {
		variables.add(v);
		switch(v.category) {
			case Variable.PARAMETER :
				parameterVariables.add(v);
				notDecisionVariables.add(v);
				break;
			case Variable.DECISION :
				decisionVariables.add(v);
				break;
			case Variable.CONSTANT :
				constants.add(v);
				notDecisionVariables.add(v);
				break;
			default : 	
				return;//TODO catch this error
		}
		switch(v.type) {
			case Expression.INTEGER :
				intVariables.add(v);
				intAndBoolVariables.add(v);
				if(v.category!= Variable.DECISION) intNotDecisionVariables.add(v);
				break;
			case Expression.BOOLEAN :
				boolVariables.add(v);
				intAndBoolVariables.add(v);
				if(v.category!= Variable.DECISION) boolNotDecisionVariables.add(v);
				break;
			case Expression.MATRIX :
				matrixVariables.add(v);
				if(v.category!=Variable.DECISION) matrixNotDecisionVariables.add(v);

				int dimensions = 0;
				switch(v.category) {
					case Variable.PARAMETER:
					case Variable.QUANTIFIER:
					case Variable.DECISION:
						dimensions = v.d.matrixDomain.numDimensions;
						break;
					case Variable.CONSTANT:
						dimensions = ((MatrixExpression) v.e).numDimensions();
						break;
					default:
						return;//TODO catch this
				}
				if(dimensions == 1) {
					oneDimensionalMatrixVariables.add(v);
					if(v.category!= Variable.DECISION) oneDimensionalNotDecisionMatrixVariables.add(v);
				}
				int baseType = 0;
				switch(v.category) {
					case Variable.PARAMETER:
					case Variable.QUANTIFIER:
					case Variable.DECISION:
						baseType = v.d.matrixDomain.baseType;
						break;
					case Variable.CONSTANT:
						baseType = ((MatrixExpression) v.e).baseType();
						break;
					default:
						return;//TODO catch this
				}

				switch(baseType) {//fix this
					case Expression.INTEGER :
						intMatrixVariables.add(v);
						intAndBoolMatrixVariables.add(v);
						if(!(v.category == Variable.DECISION)) {
							intMatrixNotDecisionVariables.add(v);
							intAndBoolNotDecisionMatrixVariables.add(v);
						}
						break;
					case Expression.BOOLEAN :
						booleanMatrixVariables.add(v);
						intAndBoolMatrixVariables.add(v);
						if(!(v.category == Variable.DECISION)) {
							booleanMatrixNotDecisionVariables.add(v);
							intAndBoolNotDecisionMatrixVariables.add(v);
						}
						break;
					default :
						return;//TODO catch this
				}
				break;
			default : 	
				return;//TODO catch this error
		}
		if(v.category!= Variable.DECISION && v.type != Expression.MATRIX) intAndBoolNotDecisionVariables.add(v);
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			for(int i = 0; i < constantDefinitions.size();i++) {
				children.add(constantDefinitions.get(i));
			}
			for(int i = 0; i < parameterDeclarations.size();i++) {
				children.add(parameterDeclarations.get(i));
			}
			for(int i = 0; i < decisionDeclarations.size();i++) {
				children.add(decisionDeclarations.get(i));
			}
			if(objective!= null) children.add(objective);
			if(constraints!=null) children.add(constraints);
		}
		return children;
	}

	/*
		Generation
	*/	

	public static Model genModel() {
		Model model = new Model();
		ArrayList<Variable> variablesInScope = new ArrayList<Variable>();
		
		//add some constants
		for(int i = 0; i < r.nextInt(Configurations.MAX_REPETITIONS);i++) {
			VariableDeclaration vd = VariableDeclaration.genConstantDefinition(variablesInScope);
			model.addVariableDeclaration(vd);
			variablesInScope.add(vd.v);
		}

		//add some parameters
		for(int i = 0; i < 0;i++) {//disabled for now...
			VariableDeclaration vd = VariableDeclaration.genParameterVariableDeclaration(variablesInScope);
			model.addVariableDeclaration(vd);
		}

		//add some decision variables
		for(int i = 0; i < r.nextInt(Configurations.MAX_REPETITIONS);i++) {
			VariableDeclaration vd = VariableDeclaration.genDecisionVariableDeclaration(variablesInScope);
			model.addVariableDeclaration(vd);
			for(int j = 0; j < vd.decisions.size(); j++) {
				variablesInScope.add(vd.decisions.get(j));
			}
		}

		//add objective
		if(r.nextBoolean()) {
			model.objective = Objective.genObjective(variablesInScope);
		}
		model.constraints = Constraints.genConstraints(variablesInScope);
		return model;
	}
}