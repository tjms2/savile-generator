package savile;
import java.util.ArrayList;
public class VariableDeclaration extends Token {

	int category;
	public ArrayList<Variable> decisions;
	public Variable v;
	public DomainExpression d;
	public Expression e;//if this is a constant definition

	//for parameters
	public VariableDeclaration(Variable v, DomainExpression d) {//out of date and needs updated for multiple variables
		category = Variable.PARAMETER;
		this.v = v;
		this.d = d;
	}
	//for constants
	public VariableDeclaration(Variable v, Expression e) {
		category = Variable.CONSTANT;
		this.v = v;
		this.e = e;
	}
	//for decision variables
	public VariableDeclaration(ArrayList<Variable> vs, DomainExpression d) {
		category = Variable.DECISION;
		this.decisions = vs;
		this.d = d;
	}

	public void printDeclaration() {
		System.out.println(toString());
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		//TODO what if it is a quantifier?
		switch(category) {
			case Variable.PARAMETER :
				return "given " + v.name + " : " + d.toString();
			case Variable.DECISION :
				return "find " + decisionsToString() + " : " + d.toString();
			case Variable.CONSTANT :
				return "letting " + v.name + " = " + e.toString();
			default :
				return "ERROR";//TODO actually catch this error
		}
	}

	public String decisionsToString() {
		String str = "";
		for(int i = 0; i < decisions.size();i++){
			if(i!=0) str += ",";
			str += decisions.get(i).name;
		}
		return str;
	}

	public String toDefaultString() {
		return "";//defaults to empty, removing the declaration
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			switch(category) {
				case Variable.PARAMETER :
				case Variable.DECISION :
					children.add(d);
					break;
				case Variable.CONSTANT :
					children.add(e);
					break;
				default :
					return null;//TODO actually catch this error
			}			
		}
		return children;
	}

	/*
		Generation 
	*/

	public static VariableDeclaration genParameterVariableDeclaration(ArrayList<Variable> variablesInScope) {
		Variable v = Variable.genParameter(variablesInScope);
		return new VariableDeclaration(v, v.d);
	}

	public static VariableDeclaration genDecisionVariableDeclaration(ArrayList<Variable> variablesInScope) {
		ArrayList<Variable> thedecisions = new ArrayList<Variable>();
		DomainExpression d = DomainExpression.genDomainExpression(Expression.genType(), false, variablesInScope, 0);
		for(int i = -1; i < r.nextInt(Configurations.MAX_REPETITIONS);i++) {
			Variable v = Variable.genDecisionVariable(variablesInScope, d);
			thedecisions.add(v);
		}
		return new VariableDeclaration(thedecisions, d);
	}

	public static VariableDeclaration genConstantDefinition(ArrayList<Variable> variablesInScope) {
		Variable v;
		switch(Expression.genType()) {
			case Expression.BOOLEAN :
				v = Variable.genBoolConstant(variablesInScope);
				break;
			case Expression.INTEGER :
				v = Variable.genIntConstant(variablesInScope);
				break;
			case Expression.MATRIX :
				v = Variable.genMatrixConstant(variablesInScope);
				break;
			default :
				return null;
		}
		return new VariableDeclaration(v, v.e);
	}

}