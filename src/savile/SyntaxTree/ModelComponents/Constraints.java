package savile;
import java.util.ArrayList;
public class Constraints extends Token {
	public ArrayList<BooleanExpression> constraints = new ArrayList<BooleanExpression>();

	public String toString() {
		if(defaulted) return toDefaultString();
		String str = "such that ";
		int numConstraintsPrinted = 0;
		for(int i = 0; i < constraints.size() ; i++) {
			if(!constraints.get(i).defaulted){
				if(numConstraintsPrinted!=0) str += ",";
				numConstraintsPrinted++;
				str += "\n\t" + constraints.get(i).toString();
			}
		}
		return str;
	}

	public String evaluationsToString(Substitution subs) {
		String str = "evaluations: ";
		for(int i = 0; i < constraints.size() ; i++) {
			str += "\n\t" + constraints.get(i).toString();
			str += "\n\t=" + constraints.get(i).evaluateBoolean(subs);
		}
		return str;
	}

	public boolean satisfiedBy(Substitution subs) {
		for(int i = 0; i < constraints.size();i++) {
			BooleanExpression con = constraints.get(i);
			if(!con.evaluateBoolean(subs)) return false;
		}
		return true;
	}


	public String toDefaultString() {
		return "such that";
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			for(int i = 0; i < constraints.size();i++) {
				children.add(constraints.get(i));
			}
		}
		return children;
	}

	public static Constraints genConstraints(ArrayList<Variable> variablesInScope) {
		Constraints c = new Constraints();
		for(int i = 0; i < r.nextInt(Configurations.MAX_REPETITIONS);i++) {
			c.constraints.add(BooleanExpression.genBooleanExpression(0, variablesInScope, false));
		}
		return c;
	}
}