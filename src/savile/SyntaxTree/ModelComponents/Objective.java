package savile;
import java.util.ArrayList;
public class Objective extends Token {
	public static final int MINIMISING = 0;
	public static final int MAXIMISING = 1;

	public int obj;
	public IIntExpression expression;
	public String toString() {
		if(defaulted) return toDefaultString();
		return objectiveString() + " " + expression.toString();
	}

	public String objectiveString() {
		switch(obj) {
			case MINIMISING :
				return "minimising";
			case MAXIMISING :
				return "maximising";
			default :
				return "ERROR";//catch this error
		}
	}

	public String toDefaultString() {
		return "";
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted)
			children.add(expression);
		return children;
	}

	public static Objective genObjective(ArrayList<Variable> variablesInScope) {
		Objective obj = new Objective();
		obj.obj = r.nextBoolean() ? MINIMISING : MAXIMISING;
		obj.expression = IntExpression.genIntExpression(0, variablesInScope, false);
		return obj;
	}
}