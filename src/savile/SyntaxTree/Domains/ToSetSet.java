package savile;
import java.util.ArrayList;
public class ToSetSet extends Set {
	MatrixExpression me;

	public ToSetSet(MatrixExpression m) {
		me = m;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		return "toSet( " + me.toString() + ")";
	}

	public String toDefaultString() {
		return DEFAULTSET;
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) children.add(me);
		return children;
	}

	public static ToSetSet genToSetSet(int depth, ArrayList<Variable> variablesInScope) {
		MatrixExpression m = MatrixExpression.genMatrixExpression(depth+1, variablesInScope, true, false);//must be one dimensional and must not contain decisions
		return new ToSetSet(m);
	}
}