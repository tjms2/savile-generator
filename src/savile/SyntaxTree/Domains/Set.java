package savile;
import java.util.ArrayList;
public abstract class Set extends Token {
	public static final int MATRIXSET = 1;
	public static final int DOMAINSET = 2;
	public static final int[] SETTYPES = new int[] {MATRIXSET,DOMAINSET};

	public static final String DEFAULTSET = "bool";

	public static Set genSet(int depth, ArrayList<Variable> variablesInScope) {
		switch(randSetType()) {
			case MATRIXSET:
			return ToSetSet.genToSetSet(depth, variablesInScope);
			case DOMAINSET:
			return DomainExpression.genDomainExpression(Expression.genType(), true, variablesInScope, depth);
			default:
			return null;
		}
	}

	public static int randSetType() {
		return SETTYPES[r.nextInt(SETTYPES.length)];
	}
}