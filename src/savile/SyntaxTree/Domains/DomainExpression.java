package savile;
import java.util.ArrayList;
public class DomainExpression extends Set {

	//domainTypes
	public static final int LITERAL = 0;
	public static final int INTERSECT = 1;
	public static final int UNION = 2;
	public static final int MINUS = 3;
	public static final int[] TYPES = new int[]{LITERAL,INTERSECT,UNION,MINUS};
	public int domainType;
	
	public int type;//type of literal domain
	public ArrayList<Range> ranges = new ArrayList<Range>();//if it is a literal domain
	public DomainExpression leftDomain;//if it is an intersect or union
	public DomainExpression rightDomain;//if its an intersect, union or negation
	public MatrixDomain matrixDomain;//if it is a literal matrix domain
	public void print() {
		System.out.println(toString());
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		switch(domainType) {
			case LITERAL :
				return literalToString();
			case INTERSECT :
				return "(" + leftDomain.toString() + " intersect " + rightDomain.toString() + ")";
			case UNION :
				return "(" + leftDomain.toString() + " union " + rightDomain.toString() + ")";
			case MINUS :
				return "(" + leftDomain.toString() + " - " + rightDomain.toString() + ")";
			default :
				return "ERROR";//catch this error
		}
	}

	public String literalToString() {
		switch(type) {
			case Expression.BOOLEAN :
				return "bool";
			case Expression.INTEGER :
				if(ranges.size() > 0) {
					String str = "int(";
					int numrangesPrinted = 0;
					for(int i = 0; i < ranges.size(); i ++) {
						if(!ranges.get(i).defaulted) {
							if(numrangesPrinted!=0)str+=",";
							numrangesPrinted++;
							str+=ranges.get(i).toString();
						}
					}
					return str + ")";
				} else {
					return "int";
				}
			case Expression.MATRIX :
				return matrixDomain.toString();
			default :
				return "ERROR";//TODO actually catch this error.
		}
	}

	public Integer[] union(Integer[] left, Integer[] right) {
		ArrayList<Integer> vals = new ArrayList<Integer>();
		if(left!= null)
			for(int i = 0; i < left.length; i++) {
				vals.add(left[i]);
			}
		if(right!= null)
			for(int i = 0; i < right.length; i++) {
				if(!vals.contains(right[i])) vals.add(right[i]);
			}
		if(vals.size() == 0) return null;
		return vals.toArray(new Integer[vals.size()]);
	}

	public Integer[] intersect(Integer[] left, Integer[] right) {
		ArrayList<Integer> vals = new ArrayList<Integer>();
		if(left==null || right == null) return null;
		for(int i = 0; i < left.length; i++) {
			for(int j = 0; j < right.length; j++) {
				if(left[i]==right[j]){
					if(!vals.contains(left[i])) vals.add(left[i]);
				}
			}
		}
		if(vals.size() == 0) return null;
		return vals.toArray(new Integer[vals.size()]);
	}

	public Integer[] minus(Integer[] left, Integer[] right) {
		ArrayList<Integer> vals = new ArrayList<Integer>();
		if(left == null) return null;
		for(int i = 0; i < left.length; i++) {
			vals.add(left[i]);
		}
		if(right != null)
			for(int i = 0; i < right.length; i++) {
				for(int j = 0; j < vals.size();j++) {
					if(right[i]==vals.get(j)) {
						vals.remove(j);
						break;
					}
				}
			}
		return vals.toArray(new Integer[vals.size()]);
	}

	public boolean[] unionBool(boolean[] left, boolean[] right) {
		if(left != null) return left;
		if(right!=null) return right;
		return null;
	}

	public boolean[] intersectBool(boolean[] left, boolean[] right) {
		if(left != null) return left;
		if(right!=null) return right;
		return null;
	}

	public boolean[] minusBool(boolean[] left, boolean[] right) {
		if(left != null) {
			if(right != null) {
				return null;
			} else {
				return left;
			}
		} else {
			return null;
		}
	}

	public String domainToSetString() {
		switch(type) {
			case Expression.INTEGER :
				Integer[] vals = intDomainToArray();
				if(vals!= null) {
					String str = "[";
					for(int i = 0; i < vals.length; i++) {
						if(i!=0) str += ",";
						str += vals[i];
					}
					return str + "]";
				} else {
					return "empty set";
				}
			case Expression.BOOLEAN :
				boolean[] valsb = boolDomainToArray();
				if(valsb!= null) {
					String str = "[";
					for(int i = 0; i < valsb.length; i++) {
						if(i!=0) str += ",";
						str += valsb[i];
					}
					return str + "]";
				} else {
					return "empty set";
				}
			case Expression.MATRIX :
			default :
				return "ERROR";//TODO
		}
	}

	//can return null
	public Integer[] intDomainToArray() {
		switch(domainType) {
			case LITERAL :
				return literalIntDomainToArray();
			case UNION :
				return union(leftDomain.intDomainToArray(), rightDomain.intDomainToArray());
			case INTERSECT :
				return intersect(leftDomain.intDomainToArray(), rightDomain.intDomainToArray());
			case MINUS :
				return minus(leftDomain.intDomainToArray(), rightDomain.intDomainToArray());
			default :
				return null;
		}
	}

	//can return null
	public boolean[] boolDomainToArray() {
		switch(domainType) {
			case LITERAL :
				return new boolean[]{true,false};
			case UNION :
				return unionBool(leftDomain.boolDomainToArray(), rightDomain.boolDomainToArray());
			case INTERSECT :
				return intersectBool(leftDomain.boolDomainToArray(), rightDomain.boolDomainToArray());
			case MINUS :
				return minusBool(leftDomain.boolDomainToArray(), rightDomain.boolDomainToArray());
			default :
				return null;
		}
	}

	public MatrixExpression[] matrixDomainToArray() {
		return null;//TODO
	}

	public Integer[] literalIntDomainToArray() {
		ArrayList<Integer> ints = new ArrayList<Integer>();
		for(int r = 0;r < ranges.size(); r++) {
			Integer[] range = ranges.get(r).toArray();
			if(range!=null) {
				for(int i = 0; i < range.length;i++) {
					if(!ints.contains(new Integer(range[i]))){
						ints.add(range[i]);
					}
				}
			}
		}
		if(ints.size()==0) return null;
		return ints.toArray(new Integer[ints.size()]);
	}

	public String toDefaultString() {
		switch(domainType) {
			case LITERAL :
				switch(type) {
					case Expression.INTEGER :
						return "int(1)";
					case Expression.BOOLEAN :
						return "bool";
					case Expression.MATRIX :
						return matrixDomain.toDefaultString();//is this a load of bullshit?
					default:
						return null;//CATCH THIS
				}
			case UNION :
			case INTERSECT :
			case MINUS :
				return rightDomain.toString();
			default :
				return null;
		}
		
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			switch(domainType) {
				case LITERAL :
					switch(type) {
						case Expression.INTEGER :
							for(int i = 0; i < ranges.size(); i++) {
								children.add(ranges.get(i));
							}
						case Expression.BOOLEAN :
							break;
						case Expression.MATRIX :
							return matrixDomain.children();
						default:
							return null;
					}
					break;
				case UNION :
				case INTERSECT :
				case MINUS :
					children.add(leftDomain);
					children.add(rightDomain);
					break;
				default :
					return null;
			}
		} else {
			switch(domainType) {
				case LITERAL :
					if(type==Expression.MATRIX) children.add(matrixDomain);
					break;
				case UNION :
				case INTERSECT :
				case MINUS :
					children.add(rightDomain);
					break;
				default :
					return null;
			}
		}	
		return children;
	}

	/*
		Generation
	*/

	public static DomainExpression genBoolIntDomainExpression(boolean canBeInfinite, ArrayList<Variable> variablesInScope, int depth) {
			return genDomainExpression(r.nextBoolean() ? Expression.INTEGER : Expression.BOOLEAN, canBeInfinite, variablesInScope, depth);
	}

	public static DomainExpression genDomainExpression(int type, boolean canBeInfinite, ArrayList<Variable> variablesInScope, int depth) {
		DomainExpression d = new DomainExpression();
		d.type = type;
		d.domainType = LITERAL;
		boolean expAllowed = true;
		if(Configurations.SENSIBLE_BOOLEAN_DOMAINS && type == Expression.BOOLEAN) expAllowed = false;
		if(type!=Expression.MATRIX && r.nextBoolean() && expAllowed) d.domainType = randDomainExpressionType();
		switch(d.domainType) {
			case LITERAL :
				//literal1
				switch(type) {
					case Expression.BOOLEAN :
						break;//do nothing as the boolean domain is atomic?
					case Expression.INTEGER :
						//below we add a random number of ranges, making it a normal domain type and not an expression
						for(int i = canBeInfinite ? 0 : -1 ; i < r.nextInt(Configurations.MAX_REPETITIONS) ; i++) {
							d.ranges.add(Range.genRange(variablesInScope, depth+1));
						}
						break;
					case Expression.MATRIX :
						//todo
						ArrayList<DomainExpression> matrixDimensions = new ArrayList<DomainExpression>();
						for(int i =-1;i<r.nextInt(Configurations.MAX_REPETITIONS);i++) {
							matrixDimensions.add(genBoolIntDomainExpression(false, variablesInScope, depth+1));
						}
						DomainExpression matrixBaseDomain = genBoolIntDomainExpression(canBeInfinite, variablesInScope, depth+1);
						d.matrixDomain = new MatrixDomain(matrixBaseDomain, matrixDimensions);
						break;
				}
				break;
			case INTERSECT :
			case UNION :
			case MINUS :
				switch(type){
					case Expression.BOOLEAN :
						d.leftDomain = genDomainExpression(type, canBeInfinite, variablesInScope, depth+1);
						d.rightDomain = genDomainExpression(type, canBeInfinite, variablesInScope, depth+1);
						break;
					case Expression.INTEGER :
						d.leftDomain = genBoolIntDomainExpression( canBeInfinite, variablesInScope, depth+1);
						d.rightDomain = genBoolIntDomainExpression( canBeInfinite, variablesInScope, depth+1);
						break;
				}
				break;
			default :
				break;
		}
		return d;
	}

	public static int randDomainExpressionType() {
		return TYPES[r.nextInt(TYPES.length)];
	}

}