package savile;
import java.util.ArrayList;
public class Range extends Token {
	public static final int RANGE = 0;
	public static final int VALUE = 1;

	public int rangeType;
	public IIntExpression lowerBound;
	public IIntExpression upperBound;
	public IIntExpression val;//if the range is actually just a value;

	public Range(IIntExpression val) {
		rangeType = VALUE;
		this.val = val;
	}

	public Range(IIntExpression lowerBound, IIntExpression upperBound) {
		rangeType = RANGE;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		switch(rangeType) {
			case RANGE :
				return "" + lowerBound.toString() + ".." + upperBound.toString();
			case VALUE :
				return "" + val.toString();
			default :
				return "ERROR";//TODO actually catch this error
		}
	}

	public Integer[] toArray() {
		Substitution subs = new Substitution(new ArrayList<Variable>());
		if(rangeType == VALUE) {
			IntEvaluation eval = val.evaluateInt(subs);
			if(!eval.undefined){
				return new Integer[]{eval.value};	
			} else {
				return null;
			}
		}
		IntEvaluation lowerVal = lowerBound.evaluateInt(subs);
		IntEvaluation upperVal = upperBound.evaluateInt(subs);
		if(lowerVal.undefined || upperVal.undefined) {
			return null;
		}
		int lower = lowerVal.value;
		int upper = upperVal.value;
		if(lower > upper) return null;
		Integer[] vals = new Integer[upper-lower];
		for(int i = 0;i < vals.length;i++) {
			vals[i] = lower+i;
		}
		return vals;
	}

	public String toDefaultString() {
		return "1";
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted) {
			if(rangeType == RANGE) {
				children.add(lowerBound);
				children.add(upperBound);
			} else {
				children.add(val);
			}
		}
		return children;
	}

	/*
		Generation
	*/
	
	public static Range genRange(ArrayList<Variable> variablesInScope, int depth) {
		if(r.nextBoolean()) {
			return new Range(IntExpression.genIntExpression(depth,variablesInScope, true));
		} else {
			if(Configurations.PREVENT_OUT_OF_ORDER_INTERVALS)  {
				while(true){
					IIntExpression lowerBound = IntExpression.genIntExpression(depth,variablesInScope, true);
					IIntExpression upperBound = IntExpression.genIntExpression(depth,variablesInScope, true);
					IntEvaluation lowerEvaluation = lowerBound.evaluateInt(new Substitution());
					IntEvaluation upperEvaluation = upperBound.evaluateInt(new Substitution());
					if(!lowerEvaluation.undefined&&!upperEvaluation.undefined) {
						int lower = lowerEvaluation.value;
						int upper = upperEvaluation.value;
						if(lower<upper) return new Range(lowerBound, upperBound);
					}
				}
			}
			return new Range(IntExpression.genIntExpression(depth,variablesInScope, true),IntExpression.genIntExpression(depth,variablesInScope, true));
		}
	}

}