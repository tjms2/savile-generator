package savile;
import java.util.ArrayList;
public class MatrixDomain extends Token {

	public boolean explicit;

	public DomainExpression matrixBaseDomain;
	public ArrayList<DomainExpression> matrixDimensions;
	public int baseType;//either int or bool
	public int numDimensions;

	public MatrixDomain(DomainExpression base, ArrayList<DomainExpression> matrixDimensions) {
		explicit = true;
		matrixBaseDomain = base;
		this.matrixDimensions = matrixDimensions;
		numDimensions = matrixDimensions.size();
		baseType = matrixBaseDomain.type;
	}

	public MatrixDomain(int baseType, int numDimensions) {
		explicit = false;
		this.baseType = baseType;
		this.numDimensions = numDimensions;
	}

	public String toString() {
		if(defaulted) return toDefaultString();
		if(explicit) {
			String str = "matrix indexed by [";
			for(int i = 0; i < matrixDimensions.size(); i++) {
				if(i!=0) str += ",";
				str += matrixDimensions.get(i).toString();

			}
			str += "] of " + matrixBaseDomain.toString();
			return str;
		} else {
			return "undefined matrix domain.";//TODO
		}
	}

	public String toDefaultString() {
		return "matrix indexed by [int(0,1)] of int(0,1)";//uses ints to acount for both int and boolean default values
	}

	public ArrayList<Token> children() {
		ArrayList<Token> children = new ArrayList<Token>();
		if(!defaulted && explicit) {
			children.add(matrixBaseDomain);
			for(int i = 0; i < matrixDimensions.size(); i++) {
				children.add(matrixDimensions.get(i));
			}		
		}
		return children;
	}

	/*
		Generation
	*/

	public static DomainExpression genAutoMatrixDomain(int[] domainAsArray, int baseType) {
		MatrixDomain dm = new MatrixDomain(baseType, domainAsArray.length);
		DomainExpression d = new DomainExpression();
		d.matrixDomain = dm;
		d.type = Expression.MATRIX;
		d.domainType = DomainExpression.LITERAL;
		return d;
	}
}