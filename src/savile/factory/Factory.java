package savile;
public class Factory {
	public static void main(String[] args) {
		int numTimes = 1;
		if(args.length >0) numTimes = Integer.parseInt(args[0]);
		for(int i=0;i<numTimes;i++) {
			Model m = Model.genModel();
			m.print();
		}
	}
}