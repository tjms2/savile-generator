package savile;
import java.util.Random;
import java.util.ArrayList;
public abstract class Generatable {
	/*
		This class is essentially just a few useful abstract methods used accross all AST classes when being generated
	*/
	protected static Random r = new Random();

	protected static ArrayList<Variable> copyAdd(ArrayList<Variable> vars, Variable v) {
		ArrayList<Variable> newVars = new ArrayList<Variable>();
		for(int i =0;i < vars.size();i++) {
			newVars.add(vars.get(i));
		}
		newVars.add(v);
		return newVars;
	}

	protected static ArrayList<Variable> filterDecisions(ArrayList<Variable> vars) {
		ArrayList<Variable> newVars = new ArrayList<Variable>();
		for(int i =0;i < vars.size();i++) {
			if(vars.get(i).category != Variable.DECISION) newVars.add(vars.get(i));
		}
		return newVars;
	}
}