package savile;
public class StabilityTester {
	public static void main(String[] args) {
		int numberIterations = 0;
		int numberGenerationCrashes = 0;
		int numberPrintingCrashes = 0;

		for(int i = 0; i < 1000000; i++) {//run about 100,000 to a million times to get an accurate impression of the stability. less is inaccurate and more takes a long time...
			numberIterations++;
			clear();
			p(
				"generating model "
				+ i
				+ "\ngeneration crashes: "
				+ numberGenerationCrashes
				+ "\nprinting   crashes: "
				+ numberPrintingCrashes
			);
			try {
				Model m = Model.genModel();
				try {
					String str = m.toString();
				} catch(Exception e) {
					p("Model printing failure");
					numberPrintingCrashes++;
				}
			} catch(Exception e) {
				p("Model generation failure");
				numberGenerationCrashes++;
			}
		}
		clear();
		p("" + numberIterations + " Models generated.\n\nmodel generation failure rate was:\t"
			+ (float)(((float)numberGenerationCrashes/(float)numberIterations)*(float)100) + "%");
		p("model printing failure rate was:\t"
			+ (float)(((float)numberPrintingCrashes/(float)numberIterations)*(float)100) + "%");
	}


	public static void p(String str) {
		System.out.println(str);
	}

	public static void clear() {
			p("\033[H\033[2J"); 
			System.out.flush();		
	}
}