package savile;
public class Configurations {
	
/********************************************************************************************************************************************************
		flags for the generator - can be helpful for testing certain parts of savile row, and will be used more extensively in the future (hopefully)
********************************************************************************************************************************************************/

	public static final int RECURSION_DEPTH = 3;//with a recursion level of less than 3 some expressions may never be generated
	public static final int MAX_REPETITIONS = 3;

	
	public static final boolean SENSIBLE_BOOLEAN_DOMAINS = true;//prevents boolean domain expressions, that are - while valid - usually are non-sensical 
	public static final boolean PREVENT_OUT_OF_ORDER_INTERVALS = true;//prevents useless ranges such as 4..3, that are thrown away by savile row 

	//TODO all below not yet implemented
	public static final boolean ALL_SENSIBLE_DOMAINS = false;//hard limits domains to a single, in order range.
	public static final boolean SIMPLE_MATRIX_LITERALS = false;//prevents matrix literals with more than 1 (OR 2??) dimensions
	public static final boolean PREVENT_OBVIOUS_UNDEFINED_DIVISION = false;//prevents division by 0 in obvious cases (x/false etc)
	public static final boolean PREVENT_OBVIOUS_OUT_OF_BOUND_INDEXES = false;//prevents out of bounds indexing in obvious cases

}