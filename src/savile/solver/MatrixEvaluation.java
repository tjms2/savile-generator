package savile;
public class MatrixEvaluation {
	public MatrixEvaluation[] submatrixes;//mulitidimensional matrixes defined recursively
	public IntEvaluation[] intContents;
	public boolean[] booleanContents;
	public int matrixType;//matrix boolean or integer
	public int indexType;//boolean or integer (type of this single dimension)
	public boolean defined;//if there is an undefined component then the matrix becomes undefined

	public MatrixEvaluation(MatrixEvaluation[] subms, int[] indexes) {
		defined = (subms.length == 0) ? false : true;
		for(int i = 0; i < subms.length; i++) {
			if(!subms[i].defined) defined = false;
		}
		matrixType = Expression.MATRIX;
		indexType = Expression.INTEGER;
	}

	public MatrixEvaluation(MatrixEvaluation[] subms, boolean[] indexes) {
		defined = (subms.length == 0) ? false : true;
		for(int i = 0; i < subms.length; i++) {
			if(!subms[i].defined) defined = false;
		}
		matrixType = Expression.MATRIX;
		indexType = Expression.BOOLEAN;
	}

	public MatrixEvaluation(IntEvaluation[] subms, int[] indexes) {
		defined = (subms.length == 0) ? false : true;
		matrixType = Expression.INTEGER;
		indexType = Expression.INTEGER;
	}

	public MatrixEvaluation(IntEvaluation[] subms, boolean[] indexes) {
		defined = (subms.length == 0) ? false : true;
		matrixType = Expression.INTEGER;
		indexType = Expression.BOOLEAN;
	}

	public MatrixEvaluation(boolean[] subms, int[] indexes) {
		defined = (subms.length == 0) ? false : true;
		matrixType = Expression.BOOLEAN;
		indexType = Expression.INTEGER;
	}

	public MatrixEvaluation(boolean[] subms, boolean[] indexes) {
		defined = (subms.length == 0) ? false : true;
		matrixType = Expression.BOOLEAN;
		indexType = Expression.BOOLEAN;
	}	

	public MatrixEvaluation() {
		defined = false;
	}
	public boolean getBoolean(IntEvaluation[] indexes) {
		//TODO
		return false;
	}

	public IntEvaluation getInt(IntEvaluation[] indexes){
		return null;//TODO
	}
}