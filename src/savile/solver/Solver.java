package savile;
import java.util.ArrayList;
public class Solver {
	public static Substitution subs;
	public static ArrayList<Variable> varsToSub;
	public static void main(String[] args) {
		Model m = Model.genModel();
		varsToSub = m.decisionVariables;
		subs = new Substitution(varsToSub);
		m.print();
		boolean b = substitute(0, m);
		if(b){
			System.out.println("solution found!");
			subs.print();
		} else {
			System.out.println("NO solution found!");
		}
	}

	//sets the pos'th variable to all possible values and calls itself of pos+1 for each iteration.
	//at the base case (last pos) it instead checks if the solution was found by calling check.
	public static boolean substitute(int pos, Model m) {
		if(pos == varsToSub.size()) {
			//base case where all variables have been substituted
			return check(m);
		}
		Variable current = varsToSub.get(pos);//assumes ordering
		//get domain
		DomainExpression d = current.d;
		switch(current.type) {


			case Expression.INTEGER :
				Integer[] possibleIntValuesOfCurrent = d.intDomainToArray();
				//for every value in domain
				if(possibleIntValuesOfCurrent==null) return false;
				for(int i = 0; i < possibleIntValuesOfCurrent.length;i++) {
					//substitute value
					subs.substitute(current, new IntEvaluation(possibleIntValuesOfCurrent[i]));
					//call on next variable
					if(substitute(pos+1,m)) return true;
				}
				break;


			case Expression.BOOLEAN :
				boolean[] possibleValuesOfCurrent = d.boolDomainToArray();
				if(possibleValuesOfCurrent==null) return false;
				//for every value in domain
				for(int i = 0; i < possibleValuesOfCurrent.length;i++) {
					//substitute value
					subs.substitute(current, possibleValuesOfCurrent[i]);
					//call on next variable
					if(substitute(pos+1,m)) return true;
				}
				break;


			//TODO 
			case Expression.MATRIX :
			//TODO
		}
		
		return false;//none of the branches returned true so return false
	}

	public static boolean check(Model m) {
		System.out.println("trying possibility: ");
		subs.print();
		return m.constraints.satisfiedBy(subs);
	}
}