package savile;
public class IntEvaluation {
	public boolean undefined;
	public int value;
	public IntEvaluation(int i) {
		value = i;
		undefined = false;
	}
	public IntEvaluation() {
		undefined = true;
	}
}