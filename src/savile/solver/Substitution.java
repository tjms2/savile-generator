package savile;
import java.util.HashMap;
import java.util.ArrayList;
public class Substitution {

	private ArrayList<Variable> vars;
	private HashMap<Variable,MatrixExpression> matrixSubstitutions = new HashMap<Variable,MatrixExpression>();//TODO will come back to this.
	private HashMap<Variable,IntEvaluation> intSubstitutions = new HashMap<Variable,IntEvaluation>();
	private HashMap<Variable,Boolean> boolSubstitutions = new HashMap<Variable,Boolean>();

	public Substitution(ArrayList<Variable> variables) {
		vars = variables;
	}

	public Substitution() {}

	public MatrixExpression getMatrix(Variable matrix) {
		return matrixSubstitutions.get(matrix);
	}

	public IntEvaluation getInt(Variable i) {
		//System.out.println("int key " + i.name + " is contained: " + intSubstitutions.containsKey(i));
		return intSubstitutions.get(i);
	}

	public Boolean getBoolean(Variable v) {
		//System.out.println("boolean key " + v.name + " is contained: " + boolSubstitutions.containsKey(v));
		return boolSubstitutions.get(v);
	}

	public void substitute(Variable v, IntEvaluation i) {
		//System.out.println("adding key for int: " + v.name);
		intSubstitutions.put(v, i);
	}

	public void substitute(Variable v, boolean b) {
		//System.out.println("adding key for boolean: " + v.name);
		boolSubstitutions.put(v, new Boolean(b));
	}

	public void substitute(Variable v, MatrixExpression exp) {
		matrixSubstitutions.put(v, exp);
		//TODO
	}

	public void print() {
		System.out.println(toString() + "\n");
	}

	public String toString() {
		String str = "";
		for ( int i = 0; i < vars.size();i++) {
			str += "\nletting " + vars.get(i).name + "\tbe " + getVarValString(vars.get(i));
		}
		return str;
	}

	public String getVarValString(Variable v) {
		if(!vars.contains(v)) return "error invalid variable";
		switch(v.type) {
			case Expression.INTEGER :
				return "" + intSubstitutions.get(v).value;
			case Expression.BOOLEAN :
				return "" + boolSubstitutions.get(v);
			case Expression.MATRIX :
				return matrixSubstitutions.get(v).toString();//TODO
			default :
				return "ERROR";
		}
	}
}