package savile;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
public class Storage {

	static String path = "../savileFiles/test.eprime";
	static String badModelsPath = "../savileFiles/savileCrashers/";
	public static void main(String[] args) {
		try {
			Model m = Model.genModel();
			System.out.println("Model Created:\n");
			System.out.println(m.toString()+"\n");
			storeModel(m);
		} catch (IOException e) {
			System.out.println("an io exception occured. please try again (to write good code)");
			e.printStackTrace();
		}
	}

	public static void storeModel(Model m) throws IOException {
		FileWriter writer = new FileWriter(path, false);
		PrintWriter printer = new PrintWriter(writer);
		printer.print(m.toString());
		printer.close();
	}

	public static void storeBadModel(Model m, int num)  throws IOException {
		FileWriter writer = new FileWriter(badModelsPath + num + ".eprime", false);
		PrintWriter printer = new PrintWriter(writer);
		printer.print(m.toString());
		printer.close();
	}
}