<< About This Project >>

Savile generator is an AST generator that randomly creates constraint models in the Essence Prime language, and runs them through savile row until savile row crashes. 
Once a faulty model is found the model is reduced to the crash inducing element and stored.

This project has 3 main areas of functionality:

1. Solver: 
	This was cancelled and rendered obselete, but this project contains a solver for a subset of the Essence Prime language.
	This solver is brute force, with no optimisations whatsoever, and was be used to test that solutions returned by the Savile Prime optimiser didn't differ from unoptomised models. 
	Do not expect it to be efficient! 

2. Stability Tester: 
	The generator has the capability of testing itself for stability by repeatedly generating every type of model in the language and checking that the resulting models are sensible. 
	This is used to test alterations to the generator.

3. BugFinder: 
	The main goal of the project is to stress test savile row, and this is done by the BugFinder project. 
	This repeatedly runs randomly generated 
	constraint models through savile row until savile row crashes. It then reduces the fault causing model down to the specific component that caused the error. 
	There are different configurations for the generator that can be altered in Configurations.java, but the configuration component of the project is only semi functional.

<< Running Project >>

I am a simple man, and used a MakeFile contained in src to compile and run the project. 
This project requires being placed in the same folder as SavileRow so that it can find and use SavileRow. 
There are no other dependencies.